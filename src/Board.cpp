/*
 * Board.cpp
 *
 *  Created on: 27 janv. 2012
 *      Author: perso
 */

#include "Board.h"
#include <boost/algorithm/string.hpp>
#include <log4cplus/loggingmacros.h>
#include <string>

using namespace std;
using namespace log4cplus;

extern Logger logger_;

/**
 * Create an empty board
 */
Board::Board()
    : logger_(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("Board")))
{
    clear();
}

Board::Board(const Board &b)
{
    *this=b;
}

Board::~Board()
{
    // Auto-generated destructor stub
}

Board &Board::operator=(const Board &b)
{
    player_to_move=b.player_to_move;
    white_castle_king_side=b.white_castle_king_side;
    white_castle_queen_side=b.white_castle_queen_side;
    black_castle_king_side=b.black_castle_king_side;
    black_castle_queen_side=b.black_castle_queen_side;
    en_passant_square=b.en_passant_square;
    halfmove_clock=b.halfmove_clock;
    fullmove_number=b.fullmove_number;
    white_king_pos=b.white_king_pos;
    black_king_pos=b.black_king_pos;
    memcpy(mailbox, b.mailbox, sizeof(b.mailbox));
    return *this;
}


void Board::clear()
{
    //LOG4CPLUS_TRACE( logger_, "Board::clear()" );

    for(int i=0; i<144; i++)
        mailbox[i] = BORDER;

    for(int y=1; y<=8; y++)
        for(int x=1; x<=8; x++)
            mailbox[idx(x,y)] = EMPTY;

    player_to_move          = WHITE;	// Colour of side to move
    white_castle_king_side  = 0;
    white_castle_queen_side = 0;	// White castling
    black_castle_king_side  = 0;
    black_castle_queen_side = 0;	// Black castling
    en_passant_square       = 0;		// En passant square
    halfmove_clock          = 0;		// Nb of half moves since last take or pawn move
    fullmove_number         = 1;		// Nb of white moves
    white_king_pos          = 0;
    black_king_pos          = 0;
}

/**
 * Set the standard initial position
 */
void Board::initialize()
{
    LOG4CPLUS_TRACE( logger_, "Board::initialize()" );
    set_fen( "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" );
}

std::string Board::to_string()
{
    //LOG4CPLUS_TRACE(logger_, "Board::to_string()");
    std::string out = "", token = " <- ";
    out += '\n';

    for (int y = MAX_RAW; y >= 1; y--)
    {
        for (int x = 1; x <= MAX_FILE; x++)
        {
            switch (mailbox[idx(x,y)])
            {
            case W_ROOK	:
                out += 'R';
                break;
            case W_KNIGHT :
                out += 'N';
                break;
            case W_BISHOP :
                out += 'B';
                break;
            case W_QUEEN :
                out += 'Q';
                break;
            case W_KING :
                out += 'K';
                break;
            case W_PAWN	:
                out += 'P';
                break;
            case B_ROOK :
                out += 'r';
                break;
            case B_KNIGHT :
                out += 'n';
                break;
            case B_BISHOP :
                out += 'b';
                break;
            case B_QUEEN :
                out += 'q';
                break;
            case B_KING	:
                out += 'k';
                break;
            case B_PAWN	:
                out += 'p';
                break;
            default :
                out += '.';
                break;
            }
        }

        if ((y==MAX_RAW)&&(player_to_move==BLACK))
            out += token;

        if ((y==1)&&(player_to_move==WHITE))
            out += token;

        out += '\n';
    }
    out += '\n';
    return out;
}

/**
 * A FEN "record" defines a particular game position, all in one text line and using
 * only the ASCII character set. A text file with only FEN data records should have
 * the file extension ".fen".
 * A FEN record contains six fields. The separator between fields is a space. The fields are:
 *
 * (1) Piece placement (from white's perspective). Each rank is described, starting with
 * rank 8 and ending with rank 1; within each rank, the contents of each square are
 * described from file a through file h. Following the Standard Algebraic Notation (SAN),
 * each piece is identified by a single letter taken from the standard English names
 * (pawn = "P", knight = "N", bishop = "B", rook = "R", queen = "Q" and king = "K").
 * White pieces are designated using upper-case letters ("PNBRQK") while black pieces use
 * lowercase ("pnbrqk"). Blank squares are noted using digits 1 through 8 (the number of
 * blank squares), and "/" separate ranks.
 *
 * (2) Active color. "w" means white moves next, "b" means black.
 *
 * (3) Castling availability. If neither side can castle, this is "-". Otherwise, this has
 * one or more letters: "K" (White can castle kingside), "Q" (White can castle queenside),
 * "k" (Black can castle kingside), and/or "q" (Black can castle queenside).
 *
 * (4) En passant target square in algebraic notation. If there's no en passant target square,
 * this is "-". If a pawn has just made a two-square move, this is the position "behind" the pawn.
 * This is recorded regardless of whether there is a pawn in position to make an en passant capture.
 *
 * (5) Halfmove clock: This is the number of halfmoves since the last pawn advance or capture.
 * This is used to determine if a draw can be claimed under the fifty-move rule.
 *
 * (6) Fullmove number: The number of the full move. It starts at 1, and is incremented after
 * Black's move.
 *
 * Examples:
 *
 * Here is the FEN for the starting position:
 * rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
 *
 * Here is the FEN after the move 1. e4:
 * rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1
 *
 * And then after 1. ... c5:
 * rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2
 *
 * And then after 2. Nf3:
 * rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2
 *
 */
void Board::set_fen(const std::string &fen_pos)
{
    std::string *fen = new std::string(fen_pos);
    int lg, x = 1, y = MAX_RAW, step = 1;
    int w_king = 0, b_king = 0;

    clear();
    boost::trim(*fen); 	// Remove leading & trailing ' '
    lg = fen->length();

    white_castle_king_side = 1;
    white_castle_queen_side = 1;
    black_castle_king_side = 1;
    black_castle_queen_side = 1;

    for (int i = 0; i < lg; i++)
    {
        int pos=idx(x,y);
        char current = (*fen)[i];
        switch (step)
        {
        case 1:							// (1) Piece placement
            switch (current)
            {
            case 'r':
                mailbox[pos] = B_ROOK;
                break;
            case 'n':
                mailbox[pos] = B_KNIGHT;
                break;
            case 'b':
                mailbox[pos] = B_BISHOP;
                break;
            case 'q':
                mailbox[pos] = B_QUEEN;
                break;
            case 'k':
                mailbox[pos] = B_KING;
                black_king_pos=pos;
                b_king++;
                break;
            case 'p':
                mailbox[pos] = B_PAWN;
                break;
            case 'R':
                mailbox[pos] = W_ROOK;
                break;
            case 'N':
                mailbox[pos] = W_KNIGHT;
                break;
            case 'B':
                mailbox[pos] = W_BISHOP;
                break;
            case 'Q':
                mailbox[pos] = W_QUEEN;
                break;
            case 'K':
                mailbox[pos] = W_KING;
                white_king_pos=pos;
                w_king++;
                break;
            case 'P':
                mailbox[pos] = W_PAWN;
                break;
            case '/':
                y--;
                x = 0;
                break;
            case ' ':
                break;
            default :
                std::string nb = fen->substr(i,1);
                int space = atoi(nb.c_str());
                if ((space>=1)&&(space<=8))
                    x+=space-1;
                else
                {
                    LOG4CPLUS_DEBUG(logger_, "Board::set_fen(\"" << fen_pos << "\") space=" << space);
                    LOG4CPLUS_ERROR(logger_, "set_fen: Wrong format.");
                    throw(EngineException(1,"Wrong FEN format.", ERROR_LOG_LEVEL ));
                }
                break;
            }
            x++;
            break;
        case 2:							// (2) Active color.
            switch (current)
            {
            case 'w':
                player_to_move = WHITE;
                break;
            case 'b':
                player_to_move = BLACK;
                break;
            case ' ':
                break;
            default :
            {
                LOG4CPLUS_DEBUG(logger_, "Board::set_fen(\"" << fen_pos << "\") current=" << current);
                LOG4CPLUS_ERROR(logger_, "set_fen: Wrong color to move.");
                throw(EngineException(2, "Wrong color to move.", ERROR_LOG_LEVEL ));
            }
            }
            break;
        case 3:							// (3) Castling availability.
            switch (current)
            {
            case 'K':
                white_castle_king_side = 0;
                break;
            case 'Q':
                white_castle_queen_side = 0;
                break;
            case 'k':
                black_castle_king_side = 0;
                break;
            case 'q':
                black_castle_queen_side = 0;
                break;
            }
            break;
        case 4:							// (4) En passant.
            if ((current>='a')&&(current<='h'))
            {
                int xx = (int)((*fen)[i]-'a');
                int yy = (int)((*fen)[i+1]-'1');
                if((xx>7)||(xx<0)||(yy>7)||(yy<0))
                {
                    LOG4CPLUS_DEBUG(logger_,	"Board::set_fen(\"" << fen_pos << "\"): xx=" << xx << " yy=" << yy);
                    LOG4CPLUS_ERROR(logger_, "set_fen: En passant square out of bound.");
                    throw(EngineException(3,"En passant square out of bound.", ERROR_LOG_LEVEL ));
                }
                en_passant_square = idx7(xx,yy);
                i += 2;
            }
            break;
        case 5: 						// (5) Halfmove clock.
            if (current!='-')
            {
                std::string nb;
                int j = -1;
                do
                    j++;
                while ( ((*fen)[i+j]>='0')&&((*fen)[i+j]<='9') );
                nb = fen->substr(i,j);
                i += j;
                halfmove_clock = atoi(nb.c_str());
            }
            break;
        case 6:							// (6) Fullmove number.
            if(current!='-')
            {
                std::string nb;
                int j = -1;
                do
                    j++;
                while ( ((*fen)[i+j]>='0')&&((*fen)[i+j]<='9') );
                nb = fen->substr(i,j);
                i += j;
                fullmove_number = atoi(nb.c_str());
                if (white_castle_king_side)
                    white_castle_king_side=fullmove_number;
                if (white_castle_queen_side)
                    white_castle_queen_side=fullmove_number;
                if (black_castle_king_side)
                    black_castle_king_side=fullmove_number;
                if (black_castle_queen_side)
                    black_castle_queen_side=fullmove_number;
            }
        default:
            break;
        }

        if ((*fen)[i]==' ')
            step++;
    }
    delete fen;
}

std::string Board::get_fen(void)
{
    std::string fen = "";
    int es = 0;

    // Field 1
    for (int i = 0; i < 64; i++)
    {
        int square = mailbox[short_mailbox[i]];
        if ( es && (square != EMPTY) )
        {
            fen += ('0'+es);
            es = 0;
        }
        switch (square)
        {
        case W_ROOK	:
            fen += 'R';
            break;
        case W_KNIGHT :
            fen += 'N';
            break;
        case W_BISHOP :
            fen += 'B';
            break;
        case W_QUEEN :
            fen += 'Q';
            break;
        case W_KING :
            fen += 'K';
            break;
        case W_PAWN	:
            fen += 'P';
            break;
        case B_ROOK :
            fen += 'r';
            break;
        case B_KNIGHT :
            fen += 'n';
            break;
        case B_BISHOP :
            fen += 'b';
            break;
        case B_QUEEN :
            fen += 'q';
            break;
        case B_KING	:
            fen += 'k';
            break;
        case B_PAWN	:
            fen += 'p';
            break;
        default :
            es++;
            break;
        }
        if ( (i%8) == 7 )
        {
            if (es) fen += ('0'+es);
            es = 0;
            if (i<63) fen += '/';
        }

    }

    // Field 2
    if (player_to_move==WHITE)
        fen += " w";
    else
        fen += " b";

    // Field 3
    fen += ' ';
    if (!white_castle_king_side)
        fen += 'K';

    if (!white_castle_queen_side)
        fen += 'Q';

    if (!black_castle_king_side)
        fen += 'k';

    if (!black_castle_queen_side)
        fen += 'q';
    if (white_castle_king_side && white_castle_queen_side && black_castle_king_side && black_castle_queen_side)
        fen += '-';

    // Field 4
    fen+=' ';

    if ( !(((en_passant_square>25)&&(en_passant_square<111))||(en_passant_square==0)) )
    {
        LOG4CPLUS_ERROR(logger_, "get_fen: En passant square out of bound.");
        assert(((en_passant_square>25)&&(en_passant_square<111))||(en_passant_square==0));
    }

    if (en_passant_square)
    {
        int xx = column(en_passant_square);
        int yy = raw(en_passant_square);
        fen += algebric[xx][yy];
    }
    else
        fen += '-';

    // Field 5
    fen += ' ';
    std::stringstream ss;
    ss << halfmove_clock;
    fen += ss.str();

    // Field 6
    fen += ' ';
    ss.str("");
    ss << fullmove_number;
    fen += ss.str();

    return fen;
}
