// *** ADDED BY HEADER FIXUP ***
#include <string>
// *** END ***
/*
 * XBoardInterface.cpp
 *
 *  Created on: 23 janv. 2012
 *      Author: perso
 */

#include <log4cplus/loggingmacros.h>
#include <string.h>
#include "XBoardInterface.h"

using namespace log4cplus;
extern Logger logger_;

/*!
    This command will be sent once immediately after your engine process is started.
    You can use it to put your engine into "xboard mode" if that is needed.
    If your engine prints a prompt to ask for user input, you must turn off the prompt
    and output a newline when the "xboard" command comes in.
    \fn XBoardInterface::xboard()
 */
void XBoardInterface::xboard()
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::xboard()");
    is_xboard=true;
}

/*!
    Beginning in protocol version 2 (in which N=2), this command will be sent
    immediately after the "xboard" command. If you receive some other command
    immediately after "xboard" (such as "new"), you can assume that protocol version 1
    is in use. The "protover" command is the only new command that xboard always sends
    in version 2. All other new commands to the engine are sent only if the engine first
    enables them with the "feature" command. Protocol versions will always be simple integers
    so that they can easily be compared.

    Your engine should reply to the protover command by sending the "feature" command
    (see below) with the list of non-default feature settings that you require, if any.

    Your engine should never refuse to run due to receiving a higher protocol version
    number than it is expecting! New protocol versions will always be compatible with
    older ones by default; the larger version number is simply a hint that additional
    "feature" command options added in later protocol versions may be accepted.
    \fn XBoardInterface::protover(int n)
 */
void XBoardInterface::protover(int n)
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::protover(" << n << ")");
    if (n>=2)
        output_protocol("feature playother=0 san=0 draw=0 sigint=1 sigterm=1 analyze=0 myname=\"Chess Engine\" variants=\"normal\" colors=0 ics=0 name=0 pause=0 setboard=1 done=1");
}

/*!
    Reset the board to the standard chess starting position. Set White on move.
    Leave force mode and set the engine to play Black. Associate the engine's
    clock with Black and the opponent's clock with White. Reset clocks and time
    controls to the start of a new game. Stop clocks. Do not ponder on this move,
    even if pondering is on. Remove any search depth limit previously set by the
    sd command.
    \fn XBoardInterface::new_game()
 */
void XBoardInterface::new_game()
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::new_game()");
    is_force=false;
    // Release the memory if a game has been played already
    if (my_chess_engine!=NULL)
        delete my_chess_engine;
    // Create the new Engine
    my_chess_engine = new Engine();
    // This AI performs an iterative deepening Alpha-Beta search to find the best
    // possible move at the greatest depth that can be searched in a given time.
    my_chess_engine->set_mode(TYPE,ITERATIVE);
    // Reset the board to the standard chess starting position. Set White on move.
    my_chess_engine->initialize();
    if (my_chess_engine==NULL)
    {
        LOG4CPLUS_ERROR(logger_, "XBoardInterface::new_game(): my_chess_engine is NULL");
        throw (EngineException(1,"Chess Initialization Error",log4cplus::FATAL_LOG_LEVEL));
    }
    else LOG4CPLUS_TRACE(logger_, "New Game.");
}

/*!
    Set the engine to play neither color ("force mode"). Stop clocks. The engine should
    check that moves received in force mode are legal and made in the proper turn,
    but should not think, ponder, or make moves of its own.
    \fn XBoardInterface::force()
 */
void XBoardInterface::force()
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::force()");
    is_force=true;
}

/*!
    Leave force mode and set the engine to play the color that is on move. Associate the
    engine's clock with the color that is on move, the opponent's clock with the color
    that is not on move. Start the engine's clock. Start thinking and eventually make
    a move.
    \fn XBoardInterface::go()
 */
string XBoardInterface::go()
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::go()");
    string value="";
    is_force=false;
    try
    {
        my_chess_engine->make_move();
        value="move " + my_chess_engine->get_last_move();
    }
    catch (EngineException &e)
    {
        LOG4CPLUS_DEBUG(logger_, "XBoardInterface::go(): " << e.what());
        LOG4CPLUS_ERROR(logger_, "Engine Interface: " << e.what());
    }
    output_protocol(value);
    return value;
}


/*!
    Set time controls.
    \fn XBoardInterface::level(int mps, int base, int inc)
 */
void XBoardInterface::level(int mps, int base, int inc)
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::level(" << mps << "," << base << "," << inc <<")");
    st((base*60)/mps);
}

/*!
    See below for the syntax of moves. If the move is illegal, print an error message;
    see the section "Commands from the engine to xboard". If the move is legal and in turn,
    make it. If not in force mode, stop the opponent's clock, start the engine's clock,
    start thinking, and eventually make a move.

    When xboard sends your engine a move, it normally sends coordinate algebraic notation.
    Examples:

    Normal moves:	e2e4
    Pawn promotion:	e7e8q
    Castling:	e1g1, e1c1, e8g8, e8c8
    Bughouse/crazyhouse drop:	P@h3
    ICS Wild 0/1 castling:	d1f1, d1b1, d8f8, d8b8
    FischerRandom castling:	O-O, O-O-O (oh, not zero)

    Beginning in protocol version 2, you can use the feature command to select SAN
    (standard algebraic notation) instead; for example, e4, Nf3, exd5, Bxf7+, Qxf7#, e8=Q, O-O,
    or P@h3. Note that the last form, P@h3, is a extension to the PGN standard's definition
    of SAN, which does not support bughouse or crazyhouse.

    xboard doesn't reliably detect illegal moves, because it does not keep track of
    castling unavailability due to king or rook moves, or en passant availability.
    If xboard sends an illegal move, send back an error message so that xboard can retract
    it and inform the user; see the section "Commands from the engine to xboard".
    \fn XBoardInterface::move(string mv)
 */
string XBoardInterface::move(const string &mvStr)
{
    string value="";
    list<string> moves;

    LOG4CPLUS_TRACE(logger_, "XBoardInterface::move(\"" << mvStr << "\")");


    try
    {
        my_chess_engine->get_possible_moves(moves);
        if (std::find(moves.begin(), moves.end(), mvStr) != moves.end())
        {
            my_chess_engine->set_move(mvStr);
            if (!is_force)
                value=go();
        }
        else
        {
            LOG4CPLUS_INFO(logger_, "Illegal move.");
            output_protocol("tellusererror Illegal move");
        }
    }
    catch (exception *e)
    {
        LOG4CPLUS_ERROR(logger_, "Move Error: " << e->what());
        LOG4CPLUS_DEBUG(logger_, "XBoardInterface::move(): " << e->what());
        output_protocol("tellusererror " + string(e->what()));
    }
    return value;
}

/*!
    In this command, N is a decimal number. When you receive the command, reply by
    sending the string pong N, where N is the same number you received. Important:
    You must not reply to a "ping" command until you have finished executing all
    commands that you received before it. Pondering does not count; if you receive
    a ping while pondering, you should reply immediately and continue pondering.
    Because of the way xboard uses the ping command, if you implement the other commands
    in this protocol, you should never see a "ping" command when it is your move;
    however, if you do, you must not send the "pong" reply to xboard until after you
    send your move. For example, xboard may send "?" immediately followed by "ping".
    If you implement the "?" command, you will have moved by the time you see the
    subsequent ping command. Similarly, xboard may send a sequence like "force", "new",
    "ping". You must not send the pong response until after you have finished executing
    the "new" command and are ready for the new game to start.

    The ping command is new in protocol version 2 and will not be sent unless you enable
    it with the "feature" command. Its purpose is to allow several race conditions that
    could occur in previous versions of the protocol to be fixed, so it is highly recommended
    that you implement it. It is especially important in simple engines that do not ponder
    and do not poll for input while thinking, but it is needed in all engines.
    \fn XBoardInterface::ping(int n)
 */
// TODO (perso#1#): passedByValue severity="performance". Function parameter n should be passed by reference. Parameter n is passed by value. It could be passed as a (const) reference which is usually faster and recommended in C++.
void XBoardInterface::ping(const string &n)
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::ping(\"" << n << "\")");
    output_protocol("pong " + n);
}

/*!
    The engine should limit its thinking to DEPTH ply.
    \fn XBoardInterface::sd(int depth)
 */
void XBoardInterface::sd(int depth)
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::sd(" << depth << ")");
//        my_chess_engine->set_mode(TYPE,PLY);
    my_chess_engine->set_depth(depth);
}

/*!
    Set time controls. The commands "level" and "st" are not used together.
    \fn XBoardInterface::st(int time)
 */
void XBoardInterface::st(int tm)
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::st(" << tm << ")");
    my_chess_engine->set_time((float) tm/6.0); // /60.0);
}

/*!
    The setboard command is the new way to set up positions, beginning in protocol
    version 2. It is not used unless it has been selected with the feature command.
    Here FEN is a position in Forsythe-Edwards Notation, as defined in the PGN standard.

    Illegal positions: Note that either setboard or edit can be used to send an illegal
    position to the engine. The user can create any position with xboard's Edit Position
    command (even, say, an empty board, or a board with 64 white kings and no black ones).
    If your engine receives a position that it considers illegal, I suggest that you send
    the response "tellusererror Illegal position", and then respond to any attempted move
    with "Illegal move" until the next new, edit, or setboard command.
    \fn XBoardInterface::setboard(string fen)
 */
void XBoardInterface::setboard(const string &fen)
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::setboard(" << fen << ")");
    try
    {
        my_chess_engine->get_board()->set_fen(fen);
    }
// TODO (perso#1#): Exception should be caught by reference. The exception is caught by value. It could be caught as a (const) reference which is usually recommended in C++.
    catch(const EngineException &e)
    {
        my_chess_engine->get_board()->initialize();
        LOG4CPLUS_DEBUG(logger_, "XBoardInterface::setboard(): " << e.what());
        LOG4CPLUS_ERROR(logger_, "Setboard Error: " << e.what());
        char str[160]="tellusererror ";
        strcat(str,e.what());
        output_protocol(str);
    }
}

/*!
    If the user asks to retract a move, xboard will send you the "remove" command.
    It sends this command only when the user is on move. Your engine should undo
    the last two moves (one for each player) and continue playing the same color.
    \fn XBoardInterface::remove()
 */
void XBoardInterface::remove()
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::remove()");
    try
    {
        my_chess_engine->undo_move();
        my_chess_engine->undo_move();
    }
    catch (exception *e)
    {
        LOG4CPLUS_DEBUG(logger_, "XBoardInterface::remove(): " << e->what());
        LOG4CPLUS_ERROR(logger_, "Remove Error: " << e->what());
    }
}

/*!
    If the user asks to back up one move, xboard will send you the "undo" command.
    xboard will not send this command without putting you in "force" mode first,
    so you don't have to worry about what should happen if the user asks to undo a
    move your engine made. (GNU Chess 4 actually switches to playing the opposite
    color in this case.)
    \fn XBoardInterface::undo()
 */
void XBoardInterface::undo()
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::undo()");
    try
    {
        my_chess_engine->undo_move();
    }
    catch (exception *e)
    {
        LOG4CPLUS_DEBUG(logger_, "XBoardInterface::undo(): " << e->what());
        LOG4CPLUS_ERROR(logger_, "Undo Error: " << e->what());
    }
}

/*!
    \fn XBoardInterface::interpret_command(vector<string> input)
 */
string XBoardInterface::interpret_command(vector<string> input)
{
    string command=input[0], value="", display_command="";
    for (unsigned int count=0; count<input.size(); count++) display_command=display_command+" "+input[count];
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::interpret_command(" << display_command <<")");
    if (is_move(command)) value=move(command);
    if (command=="new") new_game();
    if (command=="go") value=go();
    if (command=="st") st(parameter_to_int(input[1]));
    if (command=="sd") sd(parameter_to_int(input[1]));
    if (command=="level")
        level(parameter_to_int(input[1]),parameter_to_int(input[2]),parameter_to_int(input[3]));
    if (command=="ping") ping(input[1]);
    if (command=="protover") protover(parameter_to_int(input[1]));
    if (command=="quit") ChessProtocol::quit();
    if (command=="undo") undo();
    if (command=="force") force();
    if (command=="xboard") xboard();
    if (command=="display") display_board();
    if (command=="allmoves") all_moves();
    if (command=="fen") display_fen();
    //if (command=="evaluate") output_protocol("Static Evaluation: " + to_string(evaluate()));
    if (command=="setboard")
    {
        string param="";
        for (unsigned int count=1; count<input.size(); count++) param+=input[count]+" ";
        setboard(param);
    }
    if (command=="evaluate") evaluate();

    if ((!is_xboard)&&(command!="quit")) display_prompt();
    return value;
}

/*!
    Check whether the string is a move:
    Examples:
    Normal moves:	e2e4
    Pawn promotion:	e7e8q
    Castling:	e1g1, e1c1, e8g8, e8c8
    \fn XBoardInterface::is_move(string str)
 */
bool XBoardInterface::is_move(string str)
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::is_move(" << str << ")");
    bool result=false;
    if (((str.length()>=4)&&(str.length()<=5))&&
            (str[0]>='a')&&(str[0]<='h')&&(str[1]>='1')&&(str[1]<='8')&&
            (str[2]>='a')&&(str[2]<='h')&&(str[3]>='1')&&(str[3]<='8')) result=true;
    return result;
}

/*!
    \fn XBoardInterface::parameter_to_int(string param)
 */
int XBoardInterface::parameter_to_int(const string &param)
{
    LOG4CPLUS_TRACE(logger_, "XBoardInterface::parameter_to_int(" << param << ")");
    istringstream buffer(param);
    int num;
    buffer >> num;
    return num;
}
