#include "Board.h"
#include <boost/algorithm/string.hpp>
#include <log4cplus/loggingmacros.h>

using namespace std;
using namespace log4cplus;

extern Logger logger_;

/**
 * Evaluate the current position
 */
// TODO (lporchon#1#): To refactor Board::evaluate
int Board::evaluate()
{
    register int val = 0;

    val = is_checkmate(player_to_move) * CHANGE_SIDE;
    if (val!=0) return val;
    for (register int i = MIN_SQUARE; i <= MAX_SQUARE; i++)
    {
        register int c = mailbox[i];
        if (c!=BORDER)
        {
            val += c ; //+ square_value(i));
        }
    }
    if (is_king_checked(player_to_move))
        val += CHECK * player_to_move * CHANGE_SIDE;
    return val;
}

/**
 * Who's controling this square ? Black < 0 > White
 * @param position of the square
 */
int Board::space_value(register int pos)
{
// TODO (perso#1#): space_value returns who's controling the square

    int vector_b[]= { 11, 13, -11, -13 };
    int vector_k[]= { 10, 23, 25, 14, -10, -25, -23, -14 };
    int vector_r[]= { 12, 1, -12, -1 };
    int positional=CONTROL;
    register int i, value=0;
    int t_square;

    switch(pos)
    {
        case 65: case 66: case 77: case 78: positional=CONTROL+CENTER_CONTROL; break;
    }
    // by a pawn
    if ( (mailbox[pos+11]==B_PAWN)||(mailbox[pos+13]==B_PAWN) )
        value -= positional;
    if ( (mailbox[pos-11]==W_PAWN)||(mailbox[pos-13]==W_PAWN) )
        value += positional;

    // by a knight
    for ( i=0; i<8; i++ )
    {
        if ( mailbox[pos+vector_k[i]] == B_KNIGHT )
            value -= positional;
        if ( mailbox[pos+vector_k[i]] == W_KNIGHT )
            value += positional;
    }

    // by Bishop or Queen
    for (i=0; i<4; i++)
    {
        register int inc = vector_b[i];
        register int p = pos + inc ;

        t_square = mailbox[p];
        while ( t_square!=BORDER )
        {
            if ( t_square !=0 )
            {
                if ( (t_square==B_BISHOP) || (t_square==B_QUEEN) )
                    value -= positional;
                if ( (t_square==W_BISHOP) || (t_square==W_QUEEN) )
                    value += positional;
                break;
            }
            p += inc;
            t_square = mailbox[p];
        }
    }

    // by Rook or Queen
    for (i=0; i<4; i++)
    {
        register int inc = vector_r[i];
        register int p = pos + inc ;

        t_square = mailbox[p];
        while ( t_square!=BORDER )
        {
            if ( t_square !=0 )
            {
                if ( (t_square==B_ROOK) || (t_square==B_QUEEN) )
                    value -= positional;
                if ( (t_square==W_ROOK) || (t_square==W_QUEEN) )
                    value += positional;
                break;
            }
            p += inc;
            t_square = mailbox[p];
        }
    }
    return value;
}

int Board::is_checkmate(int color)
{
    if (is_king_checked(color))
    {
        list<string> moves;
        get_all_moves(moves);
        if (moves.empty())
            return (CHANGE_SIDE * color * CHECKMATE);
    }
    return 0;
}
