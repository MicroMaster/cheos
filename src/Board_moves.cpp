#include "Board.h"
#include <boost/algorithm/string.hpp>
#include <log4cplus/loggingmacros.h>

using namespace std;
using namespace log4cplus;

extern Logger logger_;

void Board::get_all_moves(std::list<std::string> &moves)
{
    std::list<Move> l_mv;

    generate_all_moves(l_mv);
    for (std::list<Move>::iterator mv = l_mv.begin(); mv != l_mv.end(); ++mv)
    {
        moves.push_back(move_to_string(*mv));
    }
}

/*!
 \fn Board::generate_all_moves(list<Move> moves)
 */
void Board::generate_all_moves(std::list<Move> &moves)
{
    list<int> checked_squares;
    list<Move> mv_list;

    moves.clear();
    checked_squares.clear();
    get_checked_squares(player_to_move, checked_squares);
    for (register int i=26; i<118; i++)
    {
        int sq, wsq;

        sq = mailbox[i];
        wsq = player_to_move * sq;
        if ( (sq!=BORDER)&&(wsq>EMPTY) )
        {
            switch(wsq)
            {
            case KNIGHT:
            {
                knight_moves(i, mv_list);
                break;
            }
            case BISHOP:
            {
                bishop_moves(i, mv_list);
                break;
            }
            case QUEEN:
            {
                bishop_moves(i, mv_list);
                rook_moves(i, mv_list);
                break;
            }
            case ROOK:
            {
                rook_moves(i, mv_list);
                break;
            }
            case PAWN:
            {
                pawn_moves(i, mv_list);
                break;
            }
            }
        }
    }

    if(!checked_squares.empty())
    {
        for (std::list<Move>::iterator it=mv_list.begin(); it != mv_list.end(); ++it)
        {
            if (std::find(checked_squares.begin(), checked_squares.end(), it->to) != checked_squares.end())
                moves.push_back(*it);
        }
    }
    else
        moves=mv_list;

    if (player_to_move==WHITE)
        king_moves(white_king_pos, moves);
    else
        king_moves(black_king_pos, moves);
}

inline void Board::pawn_moves(register int pos, std::list<Move> &moves)
{
    Move mv;
    register int sq=mailbox[pos];

    mv.from = pos;

    if ( ( (pos>=38)&&(pos<=45)&&(sq==W_PAWN) ) &&                  // If white pawn on 2nd raw
        ( (mailbox[pos+12]==EMPTY)&&(mailbox[pos+24]==EMPTY) ) )    // and both squares ahead are empty
        {
            // The pawn can move 2 squares ahead
            mv.to=pos+24;
            moves.push_back(mv);
        }

    if ( ( (pos>=98)&&(pos<=105)&&(sq==B_PAWN) ) &&                 // If black pawn on 7th raw
        ( (mailbox[pos-12]==EMPTY)&&(mailbox[pos-24]==EMPTY) ) )    // and both squares ahead are empty
        {
            // The pawn can move 2 squares ahead
            mv.to=pos-24;
            moves.push_back(mv);
        }

    for (int t=11; t<=13; t++)
    {
        if ( ((t==12)&&(mailbox[pos+(t*player_to_move)]==EMPTY)) || // if the square ahead is empty
                ((t!=12) && ( (player_to_move * mailbox[pos+(t*player_to_move)]<EMPTY) || // or the diag square are occupied by oponent
                             ((pos+(t*player_to_move))==en_passant_square) ) ) ) // or the diag square is an en passant square
        {
            mv.to=pos+(t*player_to_move);
            if (mailbox[mv.to]!=BORDER)             // if the destination square is still an the board
            {
                if ( (mv.to>=38)&&(mv.to<=105) )    // if destination square is not the last raw
                    moves.push_back(mv);            // then we move the pawn
                else                                // Otherwise the pawn is on the last raw
                {
                    if( mv.to>=110 )                // The white pawn get promoted
                    {
                        mv.promotion=W_QUEEN;
                        moves.push_back(mv);
                        mv.promotion=W_KNIGHT;
                        moves.push_back(mv);
                        mv.promotion=W_BISHOP;
                        moves.push_back(mv);
                        mv.promotion=W_ROOK;
                        moves.push_back(mv);
                    }
                    else                            // The black pawn get promoted
                    {
                        mv.promotion=B_QUEEN;
                        moves.push_back(mv);
                        mv.promotion=B_KNIGHT;
                        moves.push_back(mv);
                        mv.promotion=B_BISHOP;
                        moves.push_back(mv);
                        mv.promotion=B_ROOK;
                        moves.push_back(mv);
                    }
                }
            }
        }
    }
}

inline void Board::knight_moves(register int pos, std::list<Move> &moves)
{
    Move mv;
    int vect[]= { 10, 23, 25, 14, -10, -25, -23, -14 };

    mv.from=pos;
    for ( register int i=0; i<8; i++ )
    {
        register int t_square;
        mv.to=pos+vect[i];
        t_square = mailbox[mv.to];
        if ( (t_square!=BORDER)&&(player_to_move*t_square<=EMPTY) )
            moves.push_back(mv);
    }
}

inline void Board::bishop_moves(register int pos, std::list<Move> &moves)
{
    Move mv;
    int vect[]= { 11, 13, -11, -13 };
    register int t_square;

    mv.from=pos;
    for (register int i=0; i<4; i++)
    {
        mv.to=pos;
        do
        {
            mv.to += vect[i];
            t_square = mailbox[mv.to];
            if ( (t_square*player_to_move<=EMPTY) &&    // if destination square is empty or occupied by the opponent
                (t_square!=BORDER) &&                   // and the square is in the board
                (!is_checked( (player_to_move==WHITE ? white_king_pos : black_king_pos), mv.from, mv.to)) ) // the move does not discover the king
                    moves.push_back(mv);
        }
        while (t_square==EMPTY);
    }
}

inline void Board::rook_moves(register int pos, std::list<Move> &moves)
{
    Move mv;
    int vect[]= { -1, 12, 1, -12 };
    register int t_square;

    mv.from=pos;
    for (register int i=0; i<4; i++)
    {
        mv.to=pos;
        do
        {
            mv.to += vect[i];
            t_square = mailbox[mv.to];
            if ( (t_square*player_to_move<=EMPTY) &&
                (t_square!=BORDER) &&
                (!is_checked( (player_to_move==WHITE ? white_king_pos : black_king_pos), mv.from, mv.to)) )
                    moves.push_back(mv);
        }
        while (t_square==EMPTY);
    }
}

inline void Board::king_moves(register int pos, std::list<Move> &moves)
{
    Move mv;
    int vect[]= { 11, 12, 13, 1, -13, -12, -11, -1 };

    mv.from=pos;
    for (register int i=0; i<8; i++)
    {
        register int t_square;

        mv.to=pos+vect[i];
        t_square = mailbox[mv.to];
        if ( (t_square!=BORDER)&&(t_square*player_to_move<=EMPTY)&&(!is_checked(mv.to, mv.from)) )
            moves.push_back(mv);
    }
    if (player_to_move==WHITE)
    {
        if ( (!white_castle_king_side)&&(!is_checked(pos+1))&&(!is_checked(pos+2))&&(mailbox[pos+1]==EMPTY)&&(mailbox[pos+2]==EMPTY) )
        {
            mv.to = pos + 2;
            moves.push_back(mv);
        }
        if ( (!white_castle_queen_side)&&(!is_checked(pos-1))&&(!is_checked(pos-2))&&(mailbox[pos-1]==EMPTY)&&(mailbox[pos-2]==EMPTY)&&(mailbox[pos-3]==EMPTY) )
        {
            mv.to = pos - 2;
            moves.push_back(mv);
        }
    }
    else
    {
        if ( (!black_castle_king_side)&&(!is_checked(pos+1))&&(!is_checked(pos+2))&&(mailbox[pos+1]==EMPTY)&&(mailbox[pos+2]==EMPTY) )
        {
            mv.to = pos + 2;
            moves.push_back(mv);
        }
        if ( (!black_castle_queen_side)&&(!is_checked(pos-1))&&(!is_checked(pos-2))&&(mailbox[pos-1]==EMPTY)&&(mailbox[pos-2]==EMPTY)&&(mailbox[pos-3]==EMPTY) )
        {
            mv.to = pos - 2;
            moves.push_back(mv);
        }
    }

}

/**
 * Make the move mv on the board
 * @param *mv
 */
void Board::make_move(Move &mv)
{
    register int p = mailbox[mv.from];

    mv.taken = mailbox[mv.to];

    if (mv.promotion != EMPTY)
        mailbox[mv.to] = mv.promotion;
    else
        mailbox[mv.to] = p;

    mailbox[mv.from] = EMPTY;

    switch(p)
    {
    case W_PAWN:
        if (mv.to==en_passant_square)
        {
            en_passant_square = EMPTY;
            mailbox[mv.to-12] = EMPTY;
            break;
        }
        if ( ((mv.to-mv.from)==24) && ((mailbox[mv.to-1]==B_PAWN) || (mailbox[mv.to+1]==B_PAWN)) )
            en_passant_square=mv.to-12;
        break;

    case B_PAWN:
        if (mv.to==en_passant_square)
        {
            en_passant_square = EMPTY;
            mailbox[mv.to+12] = EMPTY;
            break;
        }
        if ( ((mv.from-mv.to)==24) && ((mailbox[mv.to-1]==W_PAWN) || (mailbox[mv.to+1]==W_PAWN)) )
            en_passant_square = mv.to+12;
        break;

    case W_KING:
        white_king_pos=mv.to;
        white_castle_king_side = fullmove_number;
        white_castle_queen_side = fullmove_number;
        if ( (mv.from==30)&&(mv.to==32) )
        {
            mailbox[31] = W_ROOK;
            mailbox[33] = EMPTY;
            break;
        }
        if ( (mv.from==30)&&(mv.to==28) )
        {
            mailbox[29] = W_ROOK;
            mailbox[26] = EMPTY;
        }
        break;

    case W_ROOK:
        if ( mv.from==33 )
            white_castle_king_side = fullmove_number;
        if ( mv.from==26 )
            white_castle_queen_side = fullmove_number;
        break;

    case B_KING:
        black_king_pos=mv.to;
        black_castle_king_side = fullmove_number;
        black_castle_queen_side = fullmove_number;
        if ( (mv.from==114)&&(mv.to==116) )
        {
            mailbox[115] = B_ROOK;
            mailbox[117] = EMPTY;
            break;
        }
        if ( (mv.from==114)&&(mv.to==112) )
        {
            mailbox[113] = B_ROOK;
            mailbox[110] = EMPTY;
        }
        break;

    case B_ROOK:
        if ( mv.from==117 )
            black_castle_king_side = fullmove_number;
        if ( mv.from==110 )
            black_castle_queen_side = fullmove_number;
    }

    if ( player_to_move == BLACK )
        fullmove_number++;

    if ( (p!=W_PAWN) && (p!=B_PAWN) )
    {
        en_passant_square = EMPTY;
        halfmove_clock++;
    }
    else
        halfmove_clock=0;

    player_to_move *= CHANGE_SIDE;
}

/**
 * Unmake the move mv on the board
 * @param *mv
 */
void Board::undo_move(const Move &mv)
{
    register int p = mailbox[mv.to];

    mailbox[mv.from] = p;
    mailbox[mv.to] = mv.taken;

    if (white_castle_king_side > fullmove_number)
        white_castle_king_side = 0;
    if (white_castle_queen_side > fullmove_number)
        white_castle_queen_side = 0;
    if (black_castle_king_side > fullmove_number)
        black_castle_king_side = 0;
    if (black_castle_queen_side > fullmove_number)
        black_castle_queen_side = 0;

    player_to_move *= CHANGE_SIDE;

    if ( (player_to_move==BLACK) && (fullmove_number>0) )
        fullmove_number--;
    if ( (p!=W_PAWN) && (p!=B_PAWN) && (halfmove_clock>0) )
        halfmove_clock--;

    switch(p)
    {
    case W_PAWN:
        // undo en passant
        if ( ((mv.to-mv.from==11)||(mv.to-mv.from==13) ) && (mv.taken==EMPTY) && (mv.from>73) && (mv.from<82) )
        {
            mailbox[mv.to-12] = B_PAWN;
            en_passant_square = mv.to;
        }
        if(mv.to-mv.from==24)
            en_passant_square=0;
        break;

    case B_PAWN:
        // undo en passant
        if ( ((mv.to-mv.from==-11)||(mv.to-mv.from==-13)) && (mv.taken==EMPTY) && (mv.from>61) && (mv.from<70) )
        {
            mailbox[mv.to+12] = W_PAWN;
            en_passant_square = mv.to;
        }
        if(mv.to-mv.from==-24)
            en_passant_square=0;
        break;

    case W_KING:
        white_king_pos=mv.from;
        if ( (mv.from==30)&&(mv.to==32) )
        {
            white_castle_king_side = 0;
            white_castle_queen_side = 0;
            mailbox[33] = W_ROOK;
            mailbox[31] = EMPTY;
            break;
        }
        if ( (mv.from==30)&&(mv.to==28) )
        {
            white_castle_king_side = 0;
            white_castle_queen_side = 0;
            mailbox[26] = W_ROOK;
            mailbox[29] = EMPTY;
        }
        break;

    case W_ROOK:
        if ( (fullmove_number==white_castle_queen_side)&&(mv.from==26) )
            white_castle_queen_side = 0;
        if ( (fullmove_number==white_castle_king_side)&&(mv.from==33) )
            white_castle_king_side = 0;
        break;

    case B_KING:
        white_king_pos=mv.from;
        if ( (mv.from==114)&&(mv.to==116) )
        {
            black_castle_king_side = 0;
            black_castle_queen_side = 0;
            mailbox[117] = B_ROOK;
            mailbox[115] = EMPTY;
            break;
        }
        if ( (mv.from==114)&&(mv.to==112) )
        {
            black_castle_king_side = 0;
            black_castle_queen_side = 0;
            mailbox[110] = B_ROOK;
            mailbox[113] = EMPTY;
        }
        break;

    case B_ROOK:
        if ( (fullmove_number==black_castle_queen_side)&&(mv.from==110) )
            black_castle_queen_side = 0;
        if ( (fullmove_number==black_castle_king_side)&&(mv.from==117) )
            black_castle_king_side = 0;
    }

    // White pawn promotion
    if (mv.promotion>0)
    {
        mailbox[mv.from] = W_PAWN;
        return;
    }

    // Black pawn promotion
    if (mv.promotion<0)
    {
        mailbox[mv.from] = B_PAWN;
        return;
    }
}

/**
 * Get the square list that might be used to protect the king if he is checked
 */
void Board::get_checked_squares(register int player, list<int> &checked_squares)
{
    int vector_b[]= { 11, 13, -11, -13 };
    int vector_k[]= { 10, 23, 25, 14, -10, -25, -23, -14 };
    int vector_r[]= { 12, 1, -12, -1 };
    register int t_square, pos;

    if(player==WHITE)
        pos=white_king_pos;
    else
        pos=black_king_pos;

    // by a pawn
    if (mailbox[pos+(player*11)]*player==B_PAWN)
        checked_squares.push_back(pos+(player*11));
    if (mailbox[pos+(player*13)]*player==B_PAWN)
        checked_squares.push_back(pos+(player*13));
    // by a knight
    for ( register int i=0; i<8; i++ )
    {
        if ( mailbox[pos+vector_k[i]] * player_to_move == B_KNIGHT )
            checked_squares.push_back(pos+vector_k[i]);
    }

    // by Bishop or Queen
    for (register int i=0; i<4; i++)
    {
        register int inc = vector_b[i];
        register int p = pos + inc ;
        list<int> vect;

        t_square = mailbox[p] * player_to_move;
        while ( (t_square!=(BORDER * player_to_move))&&(t_square<=EMPTY) )
        {
            vect.push_back(p);
            if ( (t_square==B_BISHOP)||(t_square==B_QUEEN) )
                checked_squares.merge(vect);
            p += inc;
            t_square = mailbox[p] * player_to_move;
        }
    }

    // by Rook or Queen
    for (register int i=0; i<4; i++)
    {
        register int inc = vector_r[i];
        register int p = pos + inc ;
        list<int> vect;

        t_square = mailbox[p] * player_to_move;
        while ( (t_square!=(BORDER * player_to_move))&&(t_square<=EMPTY) )
        {
            vect.push_back(p);
            if ( (t_square==B_ROOK)||(t_square==B_QUEEN) )
                checked_squares.merge(vect);
            p += inc;
            t_square = mailbox[p] * player_to_move;
        }
    }
}

/**
 * This square is it attacked ? true=yes
 * @param position of the square
 */
bool Board::is_checked(int pos, int from, int to)
{
    int vector_b[]= { 11, 13, -11, -13 };
    int vector_k[]= { 10, 23, 25, 14, -10, -25, -23, -14 };
    int vector_r[]= { 12, 1, -12, -1 };
    register int i,j;
    int t_square;

    // by a pawn
    i=pos+(player_to_move*11);
    j=pos+(player_to_move*13);
    if ( ((mailbox[i]*player_to_move==B_PAWN) && (to != i)) ||
         ((mailbox[j]*player_to_move==B_PAWN) && (to != j)) )
        return true;

    // by a knight
    for ( i=0; i<8; i++ )
    {
        j=pos+vector_k[i];
        if ( (mailbox[j] * player_to_move == B_KNIGHT) && (to != j) )
            return true;
    }

    // by Bishop or Queen
    for (i=0; i<4; i++)
    {
        register int inc = vector_b[i];
        register int p = pos + inc ;

// TODO (perso#1#): To fix: allow the checking piece to be taken. If p==to try the next vector.
        if ( (p==from)||(p==to) ) p += inc;
        t_square = mailbox[p] * player_to_move;
        while ( (t_square!=(BORDER * player_to_move)) && (t_square<=EMPTY) )
        {
            if ( (t_square==B_BISHOP) || (t_square==B_QUEEN) )
                return true;
            p += inc;
            if ( (p==from)||(p==to) ) p += inc;
            t_square = mailbox[p] * player_to_move;
        }
    }

    // by Rook or Queen
    for (i=0; i<4; i++)
    {
        register int inc = vector_r[i];
        register int p = pos + inc ;

        if ( (p==from)||(p==to) ) p += inc;
        t_square = mailbox[p] * player_to_move;
        while ( (t_square!=(BORDER * player_to_move))&&(t_square<=EMPTY) )
        {
            if ( (t_square==B_ROOK)||(t_square==B_QUEEN) )
                return true;
            p += inc;
            if ( (p==from)||(p==to) ) p += inc;
            t_square = mailbox[p] * player_to_move;
        }
    }
    return false;
}

bool Board::is_king_checked(int color)
{
    if(color==WHITE)
        return is_checked(white_king_pos);
    else
        return is_checked(black_king_pos);
}

