/*
 * EngineException.cpp
 *
 *  Created on: 23 janv. 2012
 *      Author: perso
 */

#include "EngineException.h"

EngineException::EngineException(int number=0,const char* phrase="", log4cplus::LogLevel level=0) throw()
            :m_number(number),m_phrase(phrase),m_level(level) {}

