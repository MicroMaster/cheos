//============================================================================
// Name        : chess_engine.cpp
// Author      : Luc Porchon
// Version     :
// Copyright   : GNU General Public License
// Description : chess_engine
//============================================================================

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "main.h"
#include "XBoardInterface.h"

using namespace std;
using namespace log4cplus;
using namespace boost;

Logger logger_(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("Main")));

void tokenize(const string& str, vector<string>& tokens)
{
    split_regex(tokens, str, regex(" ") );
}

#ifndef TEST

void usage(char *progname)
{
    //cout << APPLICATION_NAME << endl;
    cout << "Usage: " << progname << " <options>" << endl;
    cout << "\twhere <options> are:" << endl;
    cout << "\t\t-h\t\t\tTo get this help." << endl;
    cout << "\t\t-t <filename>\tpath to the test file containing commands." << endl;
    cout << "\t\t-v\t\t\tShows the program version number" << endl;
    exit(0);
}

int main(int argc, char *argv[])
{
    XBoardInterface interpretor;				// Implement the Xboard protocol
    vector<string> command;
    string input;
    FILE *file_in;
    char in_buff[80];
    int c;

    ::log4cplus::initialize();
    ::log4cplus::PropertyConfigurator::doConfigure("log_configure.ini");

    LOG4CPLUS_INFO(logger_, LICENSE);
    LOG4CPLUS_INFO(logger_, "Chess Engine is starting");

    file_in = stdin;

    while ((c = getopt(argc, argv,"ht:v")) != -1)
    {
        switch (c)
        {
        case 'h':
            usage(argv[0]);
            break;
        case 't':
            file_in=fopen(optarg, "r");
            if (file_in == NULL) file_in = stdin;
            break;
        case 'v':
            cout << LICENSE << endl;
            exit(0);
        default:
            file_in = stdin;
        }
    }

    setbuf(file_in, NULL);
    try
    {
        interpretor.set_output_stream(cout);
        interpretor.new_game();
        interpretor.display_prompt();
    }
    catch (std::exception *e)
    {
        LOG4CPLUS_DEBUG(logger_, "main()->Interpretor Initialization Error: " << e->what());
        LOG4CPLUS_FATAL(logger_, "Chess Engine Error: " << e->what());
        return EXIT_FAILURE;
    }

    do
    {
        command.clear();

        if(fgets(in_buff, 80, file_in)==NULL)
            exit(-1);
        string str_buff(in_buff);
        input = str_buff.substr(0,str_buff.find('\n'));

        tokenize(input, command);
        if (command.size() > 0)
        {
            LOG4CPLUS_TRACE(logger_, input);
            try
            {
                interpretor.interpret_command(command);
            }
            catch (std::exception *e)
            {
                LOG4CPLUS_DEBUG(
                    logger_,
                    "main()->interpretor.interpret_command(" << command[1] << "): " << e->what());
                LOG4CPLUS_ERROR(logger_, "Error: " << e->what());
                try
                {
                    interpretor.new_game();
                }
                catch (std::exception *e)
                {
                    LOG4CPLUS_DEBUG(logger_,
                                    "main()->interpretor.new_game(): " << e->what());
                    LOG4CPLUS_FATAL(logger_,
                                    "Chess Engine Error: " << e->what());
                    fclose(file_in);
                    return EXIT_FAILURE;
                }
            }
        }
    }
    while (input != "quit");
    LOG4CPLUS_INFO(logger_, "Bye");
    fclose(file_in);
    return EXIT_SUCCESS;
}
#endif
