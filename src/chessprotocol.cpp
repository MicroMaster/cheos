/*
 * chessprotocol.cpp
 *
 *  Created on: 22 janv. 2012
 *      Author: perso
 */
#include "chessprotocol.h"
#include <log4cplus/loggingmacros.h>

extern log4cplus::Logger logger_;

using namespace std;

ChessProtocol::ChessProtocol()
{
    output_command = NULL;
    my_output_stream = NULL;
    my_chess_engine = NULL;
}

 ChessProtocol::~ChessProtocol() {
     quit();
 }

/*!
    The chess engine should immediately exit. This command is used when xboard is
    itself exiting, and also between games if the -xreuse command line option is
    given (or -xreuse2 for the second engine).
    \fn ChessProtocol::quit()
 */
void ChessProtocol::quit()
{
    LOG4CPLUS_TRACE(logger_, "ChessProtocol::quit()");
//    if (my_chess_engine!=NULL)
//        delete my_chess_engine;
}

/*!
 \fn ChessProtocol::set_output_stream(ostream o)
 */
void ChessProtocol::set_output_stream(ostream &o)
{
    LOG4CPLUS_TRACE(logger_,
                    "void ChessProtocol::set_output_stream(ostream *o)");
    my_output_stream = (ostream *) &o;
}

/*!
 \fn ChessProtocol::display_prompt()
 */
void ChessProtocol::display_prompt()
{
    LOG4CPLUS_TRACE(logger_, "ChessProtocol::display_prompt()");
    try
    {
        display_board();
        if (my_chess_engine->get_board()->player_to_move==WHITE)
            *my_output_stream << "white> ";
        else
            *my_output_stream << "black> ";
    }
    catch (EngineException *e)
    {
        LOG4CPLUS_ERROR(logger_, "ChessProtocol::display_prompt(): " << e->what());
    }
}

/*!
 \fn ChessProtocol::display_board()
 */
void ChessProtocol::display_board()
{
    LOG4CPLUS_TRACE(logger_, "ChessProtocol::display_board()");
    try
    {
        *my_output_stream << my_chess_engine->get_board()->to_string();
    }
    catch (EngineException *e)
    {
        LOG4CPLUS_ERROR(logger_, "ChessProtocol::display_board(): " << e->what());
    }
}

/*!
 \fn ChessProtocol::display_fen()
 */
void ChessProtocol::display_fen()
{
    LOG4CPLUS_TRACE(logger_, "ChessProtocol::display_fen()");
    try
    {
        output_protocol(my_chess_engine->get_board()->get_fen());
    }
    catch (EngineException *e)
    {
        LOG4CPLUS_ERROR(logger_, "ChessProtocol::display_fen(): " << e->what());
    }
}

/*!
 \fn ChessProtocol::evaluate()
 */
int ChessProtocol::evaluate()
{
    LOG4CPLUS_TRACE(logger_, "ChessProtocol::evaluate()");
    int value = my_chess_engine->get_evaluation();
    output_protocol(to_string(value) + "\n");
    return value;
}

/*!
 \fn ChessProtocol::all_moves()
 */
void ChessProtocol::all_moves()
{
    LOG4CPLUS_TRACE(logger_, "ChessProtocol::all_moves()");
    Board current_board;
    list<string> moves;
    string move_list="";

    try
    {
        unsigned int count = 0;

        my_chess_engine->get_possible_moves(moves);
        for (list<string>::iterator i = moves.begin(); i != moves.end(); ++i)
        {
            if (++count<moves.size()) move_list+=*i+',';
            else move_list+=*i;
        }
        output_protocol(move_list);
    }
    catch (EngineException *e)
    {
        LOG4CPLUS_ERROR(logger_,
                        "ChessProtocol::display_board(): " << e->what());
    }
}

/*!
    \fn ChessProtocol::output_protocol(string st)
 */
// TODO (perso#1#): passedByValue: severity="performance" Function parameter st should be passed by reference. Parameter st is passed by value. It could be passed as a (const) reference which is usually faster and recommended in C++.
void ChessProtocol::output_protocol(const string &st)
{
    LOG4CPLUS_TRACE(logger_, "ChessProtocol::output_protocol(" << st << ")");
    try
    {
        my_result=st;
        *my_output_stream << st << endl;
        my_output_stream->flush();
    }
    catch (exception *e)
    {
        LOG4CPLUS_DEBUG(logger_, "ChessProtocol::output_protocol(): " << e->what());
        LOG4CPLUS_ERROR(logger_, "Output Access Error: " << e->what());
    }
}
