/*
 * Engine.cpp
 *
 *  Created on: 24 janv. 2012
 *      Author: perso
 */
#include <string.h>
#include <cstdlib>
#include <stdlib.h>
#include "Engine.h"
#include <sl/sl.h>
#include <log4cplus/loggingmacros.h>

using namespace log4cplus;
extern Logger logger_;
using namespace std;

Engine::Engine(const Engine &e)
{
        *this = e;
}

Engine::Engine()
{
    LOG4CPLUS_TRACE(logger_, "Engine::Engine()");
    my_game = ggtl_new();
    if (my_game == NULL)
        throw(manage_error("Engine::Engine()", "Bad allocation", 1, FATAL_LOG_LEVEL));
    try
    {
        if (my_board != NULL) delete my_board;
        my_board = new Board();
        my_pointer_to_board = (void *) my_board;
    }
    catch (exception *e)
    {
        // free the resource already set
        ggtl_free(my_game);
        if (my_board != NULL)
            delete my_board;
        throw(manage_error("Engine::Engine()", "While creating the board", 1, FATAL_LOG_LEVEL));
    }
    set_mode(TRACE, 5);
    set_mode(TYPE, ITERATIVE);
    set_time(1.00); // Default = 1s
}

Engine::~Engine()
{
    LOG4CPLUS_TRACE(logger_, "Engine::~Engine()");
}

Engine &Engine::operator=(const Engine &e)
{
    my_board=e.my_board;
    try
    {
        my_game = ggtl_new();
        if (my_game == NULL)
            throw(manage_error("Engine::new_game()", "Bad allocation", 2, FATAL_LOG_LEVEL));
        initialize();
    }
    catch (EngineException *e)
    {
        throw(manage_error("Engine::new_game()", "Unable to set up the board"));
    }
    catch (exception *e)
    {
        throw(manage_error("Engine::new_game()", "While creating a new game"));
    }
    return *this;
}


void Engine::new_game()
{
    LOG4CPLUS_TRACE(logger_, "Engine::new_game()");
    try
    {
        free(); // we release its memory if a game has been played
        my_game = ggtl_new();
        if (my_game == NULL)
            throw(manage_error("Engine::new_game()", "Bad allocation", 2, FATAL_LOG_LEVEL));
        initialize();
    }
    catch (EngineException *e)
    {
        throw(manage_error("Engine::new_game()", "Unable to set up the board"));
    }
    catch (exception *e)
    {
        throw(manage_error("Engine::new_game()", "While creating a new game"));
    }
}

/**
 * Set the starting position to initial_position.
 * Returns 0 on success, -1 on failure.
 * @param *fenPosition
 */
void Engine::initialize()
{
    LOG4CPLUS_TRACE(logger_,"Engine::initialize()");

    my_board->initialize(); // takes the fen string into account
    if (ggtl_init(my_game, (void *) my_board) == NULL)
        throw(manage_error("Engine::initialize(string fenPosition)",
                           "Unable to initialize the board"));
    else
    {
        // set the callback functions
        ggtl_vtab(my_game)->move = wrapper_to_call_make_move;
        ggtl_vtab(my_game)->unmove = wrapper_to_call_undo_move;
        ggtl_vtab(my_game)->get_moves = wrapper_to_call_generate_moves;
        ggtl_vtab(my_game)->eval = wrapper_to_call_evaluate;
        ggtl_vtab(my_game)->game_over = wrapper_to_call_game_over;
    }
}

/**
 * Free memory
 */
void Engine::free()
{
    LOG4CPLUS_TRACE(logger_, "Engine::free()");
    if (my_game != NULL) // if a game has been played
        ggtl_free(my_game);
}

/**
 * Return the current board
 */
Board &Engine::set_move(const string &new_move)
{
    LOG4CPLUS_TRACE(logger_, "Engine::set_move(" << new_move << ")");
    Move *current_move=string_to_move(new_move);
    Board *board = static_cast<Board *>( ggtl_move(my_game, static_cast<void *>(current_move) ) );
    if (board == NULL)
        throw(manage_error("Engine::set_move(new_move)", "Illegal move"));
    return (Board&) *board;
}

/**
 * Apply the given move to the current position, a pointer to
 * the resulting Board is returned on success, NULL on failure.
 * Upon success, the move is stored in a history list.
 * If the unmove() callback is not provided, the previous state
 * is also stored in order to provide undo.
 * @param *new_move
 */
Board *Engine::get_board()
{
    LOG4CPLUS_TRACE(logger_, "Engine::get_board()");
    return my_board;
}


/**
 * Make the Engine perform a move. Returns a pointer to the new
 * game Board, or NULL on error. The same internal mechanisms are
 * used as for the set_move(), so the AI's moves can also be undone.
 */
Board &Engine::make_move()
{
    LOG4CPLUS_TRACE(logger_, "Engine::make_move()");
    Board *board = static_cast<Board *>( ggtl_ai_move(static_cast<GGTL *>(my_game)) );
    if (board == NULL)
    {
/*        if (is_game_over())
            throw(manage_error("Engine::make_move()",
                               "The game is over. Please, make a new one"));
        else
            throw(manage_error("Engine::make_move()",
                               "AI problem while computing moves", 3, FATAL_LOG_LEVEL));
*/    }
    return (Board &) *board;
}

/**
 * Reverts the internal game state to the previous. Returns a
 * pointer to the new current Board, or NULL on error
 *(e.g. if there was no move to undo).
*/
Board &Engine::undo_move()
{
    LOG4CPLUS_TRACE(logger_, "Engine::undo_move()");
    Board *board = static_cast<Board *>( ggtl_undo(my_game) );
    if (board == NULL)
        throw(manage_error("Engine::undo_move()", "While undoing a move", 4,
                           FATAL_LOG_LEVEL));
    return static_cast<Board &>( *board );
}

/**
 * Sets and gets the value of the given key. The following
 * keys are available:
 *
 * TYPE - the type of AI to use
 *    See ggtlai(3) for a description of the various types supported.
 *
 * MSEC - The maximum time(in milliseconds) the iterative AI is allowed
 * to use for a search.
 *
 * PLY - The depth to search to for the fixed-depth AI.
 *
 * TRACE - The level of trace information to print during search.
 * The trace information will be printed to standard output. Zero(the default)
 * turns off tracing; larger numbers give more detailed trace output.
 *
 * CACHE - Decide what to cache. If 0, GGTL will not cache anything.
 * Other valid values are combinations of the following, ORed together:
 *
 *       STATES  - cache states
 *       MOVES   - cache moves
 *
 *
 *     Example: use ggtl_set(g, CACHE, STATES | MOVES) to cache both moves
 * and states(this is the default).
 *
 * VISITED - ggtl_get() only
 *    Returns the number of states visited by the last AI search, or -1 if no
 * information is available. If no search has taken place, the value is undefined.
 * PLY_REACHED - ggtl_get() only Returns the effective depth of the last
 * iterative AI search. The value is undefined if no such search has taken place.
 *
 * @param key
 * @param value
 */
void Engine::set_mode(int key, int value)
{
    LOG4CPLUS_TRACE(logger_, "Engine::set_mode(" << key << "," << value <<")");
    ggtl_set(my_game, key, value);
}

/**
 * Returns a pointer to the current Board, or NULL on error.
 *
Board &Engine::get_board() throw (EngineException) {
	LOG4CPLUS_TRACE(logger_, "Engine::get_board()");
	Board *board = (Board *) ggtl_peek_state(my_game);
	if (board == NULL)
		throw(manage_error("Engine::get_board()", "While getting the board"));
	return (Board &) *board;
}*/

/**
 * Returns a pointer to the last move performed, or NULL on error.
 */
string Engine::get_last_move()
{
    LOG4CPLUS_TRACE(logger_, "Engine::get_last_move()");
    Move *current_move = static_cast<Move *>( ggtl_peek_move(my_game) );
    if (current_move == NULL)
        throw(manage_error("Engine::get_last_move()", "While getting last move"));
    return move_to_string(*current_move);
}

/**
 * Returns a list of the available moves at the current state,
 * or NULL if there are no available moves(i.e, if the game is over).
 */

list<Move> Engine::get_possible_moves()
{
    LOG4CPLUS_TRACE(logger_, "Engine::get_possible_moves()");
    list < Move > all_moves;
    Move *current_move = NULL;
    GGTL_MOVE *moves = ggtl_get_moves(my_game);
    if (moves == NULL)
        throw(manage_error("Engine::get_possible_moves()", "Empty move list"));
    do
    {
        current_move = static_cast<Move *>( sl_pop(static_cast<void *>( moves ) ) );
        if (current_move != NULL)
            all_moves.push_back(*current_move);
    }
    while (current_move != NULL);
    return all_moves;
}

/**
 * Returns the value of the current game state.
 */
int Engine::get_evaluation()
{
    LOG4CPLUS_TRACE(logger_, "Engine::get_evaluation()");
    return ggtl_eval(my_game);
}

/**
 * Set searching time limit in milliseconds.
 * @param time
 */
void Engine::set_time(float t)
{
    LOG4CPLUS_TRACE(logger_, "Engine::set_time(" << t << "s)");
    set_mode(TYPE, ITERATIVE);
    ggtl_set_float(my_game, TIME, t);
}

/**
 * Set searching depth.
 * @param depth
 */
void Engine::set_depth(int depth)
{
    LOG4CPLUS_TRACE(logger_, "Engine::set_depth(" << depth << ")");
    set_mode(TYPE, FIXED);
    set_mode(PLY, depth);
}

/*!
 \fn Engine::allocate_move()
 */
Move *Engine::allocate_move()
{
    LOG4CPLUS_TRACE(logger_, "Engine::allocate_move()");
    Move *current_move = static_cast<Move *>( ggtl_uncache_move_raw(my_game) );
    if (!current_move) current_move = new Move;
    return current_move;
}

/*!
 \fn Engine::WrapperToCallMakeMove(void *my_pointer_to_board, void *mv, GGTL *game)
 */
void *Engine::wrapper_to_call_make_move(void *my_pointer_to_board, void *mv,
                                        GGTL *game)
{
    Move *current_move = static_cast<Move*>( mv );
    Board *mySelf = static_cast<Board *>( my_pointer_to_board );
    current_move->brd=mySelf->get_fen(); //LP
    mySelf->make_move(static_cast<Move&>( *current_move ));
    return static_cast<void *>( mySelf );
}

/*!
 \fn Engine::wrapper_to_call_undo_move(void *pt2Object, void *mv, GGTL *game)
 */
void *Engine::wrapper_to_call_undo_move(void *my_pointer_to_board, void *mv,
                                        GGTL *game)
{
    Move *current_move = static_cast<Move*>( mv );
    Board *mySelf = static_cast<Board *>( my_pointer_to_board );
    //mySelf->undo_move((Move &) *current_move);
    mySelf->set_fen(current_move->brd); //LP
    return static_cast<void *>( mySelf );
}

/*!
 \fn Engine::wrapper_to_call_generate_moves(void *my_pointer_to_board, GGTL *game)
 */
GGTL_MOVE * Engine::wrapper_to_call_generate_moves(void *my_pointer_to_board,
        GGTL *game)
{
    Board *myBoard = static_cast<Board *>( my_pointer_to_board );
    list < Move > all_moves;
    GGTL_MOVE *moves = NULL;

    myBoard->generate_all_moves(all_moves);
    if (all_moves.empty() == false)
    {
        list<Move>::iterator mv;
        for (mv = all_moves.begin(); mv != all_moves.end(); ++mv)
        {
            Move *current_move = static_cast<Move *>( ggtl_uncache_move_raw(game) );
            if (!current_move)
                current_move = new Move;
            *current_move = *mv;
            moves = static_cast<GGTL_MOVE *>( sl_push(moves, ggtl_wrap_move(game, current_move)) );
        }
    }
    else
    {
        return NULL;
    }
    return moves;
}

/*!
 \fn Engine::wrapper_to_call_evaluate(void *my_pointer_to_board, GGTL *game)
 */
int Engine::wrapper_to_call_evaluate(void *my_pointer_to_board, GGTL *game)
{
    Board* mySelf = static_cast<Board*>( my_pointer_to_board );
    return mySelf->evaluate();
}

/*!
 \fn Engine::wrapper_to_call_game_over(void *my_pointer_to_board, GGTL *game)
 */
int Engine::wrapper_to_call_game_over(void *my_pointer_to_board, GGTL *game)
{
    Board* mySelf = static_cast<Board*>( my_pointer_to_board );
    return mySelf->is_checkmate(mySelf->player_to_move);
}

/*!
 \fn Engine::manage_error(const string methodName, const string message)
 */
EngineException Engine::manage_error(const string &methodName,
                                     const string &message, int errorCode, LogLevel level)
{
    LOG4CPLUS_TRACE(logger_, "Engine::manage_error(" << methodName << ")");
    LOG4CPLUS_DEBUG(logger_,
                    "Engine error in " << methodName << ": " << message);
    LOG4CPLUS_ERROR(logger_, "Engine error: " << message);
    return (EngineException(errorCode, "Engine error", level));
}

/**
 * Convert string to move
 * Normal moves:	e2e4
 * Pawn promotion:	e7e8q
 * Castling:		e1g1, e1c1, e8g8, e8c8
 */
Move *Engine::string_to_move(const string &move_string)
{
    string column="abcdefgh";
    int x, y;
    stringstream move_stream;
    Move *move=allocate_move();

    // Translates the source square
    x=column.find(move_string[0])+1;
    move_stream << move_string[1];
    move_stream >> y;
    move->from=idx(x,y);

    // Translates the destination square
    move_stream.clear();
    x=column.find(move_string[2])+1;
    move_stream << move_string[3];
    move_stream >> y;
    move->to=idx(x,y);

    // Translates the promotion piece
    if (move_string.length()==5)
    {
        char promotion=move_string[4];
        if (promotion=='q')
        {
            if (y==8) move->promotion=W_QUEEN;
            else move->promotion=B_QUEEN;
        }
        if (promotion=='r')
        {
            if (y==8) move->promotion=W_ROOK;
            else move->promotion=B_ROOK;
        }
        if (promotion=='n')
        {
            if (y==8) move->promotion=W_KNIGHT;
            else move->promotion=B_KNIGHT;
        }
        if (promotion=='b')
        {
            if (y==8) move->promotion=W_BISHOP;
            else move->promotion=B_BISHOP;
        }
    }
    if (move_string.length()==6)
    {
        // It may be a "en passant" move
        char en_passant=move_string[4];
        if (en_passant=='e')
        {
            move->promotion=true;
        }
    }
    return(move);
}
/*
string Engine::move_to_string(const Move mv)     //e7e8q
{
    string column="abcdefgh", move_string="";
    char c;
    stringstream move_stream;

    move_string+=column[mv.from%12];
    move_stream << raw(mv.from);
    move_stream >> c;
    move_string+=c;
    move_string+=column[mv.to%12];
    move_stream.clear();
    move_stream << raw(mv.to);
    move_stream >> c;
    move_string+=c;

    if (mv.promotion)
    {
        int promotion=(mv.promotion>0)?mv.promotion:mv.promotion*-1;
        switch (promotion)
        {
        case W_QUEEN:
            move_string+='q';
            break;
        case W_ROOK:
            move_string+='r';
            break;
        case W_KNIGHT:
            move_string+='n';
            break;
        case W_BISHOP:
            move_string+='b';
            break;
        }
    }
    return move_string;
}
*/
void Engine::get_possible_moves(list<string> &moves)
{
    my_board->get_all_moves(moves);
}


