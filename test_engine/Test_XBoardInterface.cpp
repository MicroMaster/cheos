#define BOOST_TEST_MODULE chess_engine

#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/regex.hpp>

#include "XBoardInterface.h"

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/loggingmacros.h>
//#include <log4cplus/fileappender.h>

using namespace std;
using namespace log4cplus;
using namespace boost;

//Logger logger = Logger::getInstance("main");
//SharedAppenderPtr my_Appender(new FileAppender("chess_engine.log"));
//std::auto_ptr<Layout> my_layout = std::auto_ptr<Layout>(new log4cplus::SimpleLayout());

Logger logger(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("Main")));


ofstream out;

struct F
{
    F() : i( 0 )
    {
        cout << "setup" << endl;

        ::log4cplus::initialize();
        ::log4cplus::PropertyConfigurator::doConfigure("testlog_configure.ini");

        LOG4CPLUS_INFO(logger, "\n\nTest is starting");
    }
    ~F()
    {
        cout << "teardown" << endl;
    }

    int i;
};

/************************/
/* Test XBoardInterface */
/************************/

BOOST_AUTO_TEST_SUITE( test_XBoardInterface )


BOOST_FIXTURE_TEST_CASE( XBoardInterface_protover, F )
{
    LOG4CPLUS_INFO( logger,"BOOST_FIXTURE_TEST_CASE: XBoardInterface_protover");
    XBoardInterface interpretor;
    vector<string> c;

    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("protover");
    c.push_back("2");
    string result="feature playother=0 san=0 draw=0 sigint=1 sigterm=1 analyze=0 myname=\"Chess Engine\" variants=\"normal\" colors=0 ics=0 name=0 pause=0 setboard=1 done=1";
    interpretor.interpret_command(c);
    BOOST_REQUIRE_EQUAL(interpretor.get_last_result(),result);
}

BOOST_AUTO_TEST_CASE( XBoardInterface_ping )
{
    LOG4CPLUS_INFO( logger,"TEST CASE 1: XBoardInterface_ping ");
    XBoardInterface interpretor;
    vector<string> c;
    string result="pong 2";

    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("ping");
    c.push_back("2");
    interpretor.interpret_command(c);
    BOOST_REQUIRE_EQUAL(interpretor.get_last_result(),result);
    c.push_back("quit");
    interpretor.interpret_command(c);
    c.clear();
}

BOOST_AUTO_TEST_CASE(XBoardInterface_Initial_position)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 2: XBoardInterface_Initial_position");
    XBoardInterface interpretor;
    vector<string> c;
    string result="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}


BOOST_AUTO_TEST_CASE(XBoardInterface_move)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 3: XBoardInterface_move");
    XBoardInterface interpretor;
    vector<string> c;
    string result="rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq - 0 1";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e2e4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 4: XBoardInterface_moves");
    XBoardInterface interpretor;
    vector<string> c;
    string result="r1bqkbnr/pppp1ppp/2n5/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq - 2 3";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e2e4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e7e5");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("g1f3");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("b8c6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}


BOOST_AUTO_TEST_CASE(XBoardInterface_move_white_castle_king_side)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 5: XBoardInterface_move_white_castle_king_side");
    XBoardInterface interpretor;
    vector<string> c;
    string result="r1bqkbnr/ppp2ppp/2np4/1B2p3/4P3/5N2/PPPP1PPP/RNBQ1RK1 b kq - 1 4";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("r1bqkbnr/ppp2ppp/2np4/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq - 0 4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e1g1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_undo_white_castle_king_side)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 6: XBoardInterface_undo_white_castle_king_side");
    XBoardInterface interpretor;
    vector<string> c;
    string result="r1bqkbnr/ppp2ppp/2np4/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq - 0 4";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("r1bqkbnr/ppp2ppp/2np4/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq - 0 4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e1g1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_black_castle_king_side)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 7: XBoardInterface_move_black_castle_king_side");
    XBoardInterface interpretor;
    vector<string> c;
    string result="r1bq1rk1/ppp1bppp/2np1n2/1B2p3/4P3/2NP1N2/PPP2PPP/R1BQ1RK1 w - - 3 7";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e2e4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e7e5");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("g1f3");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("b8c6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("f1b5");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d7d6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e1g1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("g8f6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d2d3");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("f8e7");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("b1c3");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e8g8");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_undo_black_castle_king_side)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 8: XBoardInterface_undo_black_castle_king_side");
    XBoardInterface interpretor;
    vector<string> c;
    string result="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e2e4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e7e5");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("g1f3");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("b8c6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("f1b5");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d7d6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e1g1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("g8f6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d2d3");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("f8e7");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("b1c3");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e8g8");
    interpretor.interpret_command(c);
    c.clear();

    for (int i=1; i<=12; i++)
    {
        c.push_back("undo");
        interpretor.interpret_command(c);
        c.clear();
    }
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_white_queen_promotion)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 9: XBoardInterface_move_white_queen_promotion");
    XBoardInterface interpretor;
    vector<string> c;
    string result="Q3k3/8/8/2p5/1pP5/8/8/4K3 b - - 0 1";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("4k3/P7/8/2p5/1pP5/8/8/4K3 w - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("a7a8q");
    interpretor.interpret_command(c);
    c.clear();

    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_white_rook_promotion)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 10: XBoardInterface_move_white_rook_promotion");
    XBoardInterface interpretor;
    vector<string> c;
    string result="R3k3/8/8/2p5/1pP5/8/8/4K3 b - - 0 1";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("4k3/P7/8/2p5/1pP5/8/8/4K3 w - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("a7a8r");
    interpretor.interpret_command(c);
    c.clear();

    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_black_bishop_promotion)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 11: XBoardInterface_move_black_bishop_promotion");
    XBoardInterface interpretor;
    vector<string> c;
    string result="4k3/8/8/2p5/1pP5/8/8/4K1b1 w - - 0 2";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("4k3/8/8/2p5/1pP5/8/6p1/4K3 b - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("g2g1b");
    interpretor.interpret_command(c);
    c.clear();

    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_black_knight_promotion)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 12: XBoardInterface_move_black_knight_promotion");
    XBoardInterface interpretor;
    vector<string> c;
    string result="4k3/8/8/2p5/1pP5/8/8/4K1n1 w - - 0 2";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("4k3/8/8/2p5/1pP5/8/6p1/4K3 b - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("g2g1n");
    interpretor.interpret_command(c);
    c.clear();

    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_white_en_passant)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 13: XBoardInterface_move_white_en_passant");
    XBoardInterface interpretor;
    vector<string> c;
    string result="k7/2p5/8/3pP3/8/8/8/4K3 w - d6 0 2";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("k7/2pp4/8/4P3/8/8/8/4K3 b - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d7d5");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_white_takes_en_passant)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 14: XBoardInterface_move_white_takes_en_passant");
    XBoardInterface interpretor;
    vector<string> c;
    string result="k7/2p5/3P4/8/8/8/8/4K3 b - - 0 2";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("k7/2pp4/8/4P3/8/8/8/4K3 b - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d7d5");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e5d6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_white_undo_en_passant)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 15: XBoardInterface_move_white_undo_en_passant");
    XBoardInterface interpretor;
    vector<string> c;
    string result="k7/2p5/8/3pP3/8/8/8/4K3 w - d6 0 2";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("k7/2pp4/8/4P3/8/8/8/4K3 b - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d7d5");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e5d6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_black_en_passant)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 16: XBoardInterface_move_black_en_passant");
    XBoardInterface interpretor;
    vector<string> c;
    string result="k7/8/8/8/2pP4/8/4P3/4K3 b - d3 0 2";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("k7/8/8/8/2p5/8/3PP3/4K3 w - - 0 2");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d2d4");
    interpretor.interpret_command(c);
    c.clear();

    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_black_takes_en_passant)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 17: XBoardInterface_move_black_takes_en_passant");
    XBoardInterface interpretor;
    vector<string> c;
    string result="k7/8/8/8/8/3p4/4P3/4K3 w - - 0 3";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("k7/8/8/8/2p5/8/3PP3/4K3 w - - 0 2");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d2d4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("c4d3");
    interpretor.interpret_command(c);
    c.clear();

    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_move_black_undo_en_passant)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 18: XBoardInterface_move_black_undo_en_passant");
    XBoardInterface interpretor;
    vector<string> c;
    string result="k7/8/8/8/2pP4/8/4P3/4K3 b - d3 0 2";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("setboard");
    c.push_back("k7/8/8/8/2p5/8/3PP3/4K3 w - - 0 2");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d2d4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("c4d3");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();

    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_generate_first_white_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 19: XBoardInterface_generate_first_white_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"b1a3","b1c3","g1f3","g1h3","a2a3","a2a4","b2b3","b2b4","c2c3","c2c4","d2d3","d2d4","e2e3","e2e4","f2f3","f2f4","g2g3","g2g4","h2h3","h2h4"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_generate_first_black_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 20: XBoardInterface_generate_first_black_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"b8a6","b8c6","g8f6","g8h6","a7a6","a7a5","b7b6","b7b5","c7c6","c7c5","d7d6","d7d5","e7e6","e7e5","f7f6","f7f5","g7g6","g7g5","h7h6","h7h5"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d2d4");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_generate_rook_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 21: XBoardInterface_generate_first_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"a1b1","a1c1","a1d1","a1a2","a1a3","e1c1","d4d3","d4c4","d4b4","d4e4","d4f4","d4g4","d4h4","d4d5","d4d6","d4d7","d4d8","d2d3","a4a5","e1d1","e1e2","e1f2","e1f1"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("setboard");
    c.push_back("5k2/8/8/8/P2R4/8/3P4/R3K3 w Q - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_generate_knight_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 22: XBoardInterface_generate_first_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"b4a2","b4a6","b4c6","b4d5","b4d3","c3b2","c3b3","c3c4","c3d4","c3d3","c3d2"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("setboard");
    c.push_back("5k2/8/8/8/1N6/2K5/2P5/8 w - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_generate_bishop_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 23: XBoardInterface_generate_first_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"c3c2","c3b2","c3b3","c3b4","c3c4","c3d3","c3d2","d4c5","d4b6","d4a7","d4e5","d4f6","e3e4"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("setboard");
    c.push_back("5k2/8/5p2/8/3B4/2K1P3/8/8 w - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_generate_queen_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 24: XBoardInterface_generate_first_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"c3c2","c3b2","c3b3","c3b4","c3c4","c3d3","c3d2","d4c5","d4b6","d4a7","d4e5","d4f6","e3e4","d4c4","d4b4","d4a4","d4d5","d4d6","d4d7","d4d8","d4e4","d4f4","d4g4","d4h4","d4d3","d4d2","d4d1"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("setboard");
    c.push_back("5k2/8/5p2/8/3Q4/2K1P3/8/8 w - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}


BOOST_AUTO_TEST_CASE(XBoardInterface_generate_casteling_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 25: XBoardInterface_casteling_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"a1b1","a1c1","a1d1","e1d1","e1f1","e1c1","e1g1","h1g1","h1f1"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("setboard");
    c.push_back("4k3/8/8/8/8/p3p2p/P3P2P/R3K2R w KQ - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_generate_casteling_moves2)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 26: XBoardInterface_casteling_moves2");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"a1b1","a1c1","a1d1","e1f1","h1g1","h1f1"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("setboard");
    c.push_back("4k1r1/8/8/8/b7/p3p2p/P3P2P/R3K2R w KQ - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_generate_en_passant_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 27: XBoardInterface_en_passant_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"c5c6","c5d6","d4d5","d4e3","d4d3","d4c3","e5d6","e5e6"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("setboard");
    c.push_back("4k3/8/8/2PpP3/3K4/8/8/8 w - d6 0 2");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_undo_en_passant_moves2)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 28: XBoardInterface_undo_en_passant_moves2");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result= "4k3/8/8/2PpP3/3K4/8/8/8 w - d6 0 2";
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("setboard");
    c.push_back("4k3/8/8/2PpP3/3K4/8/8/8 w - d6 0 2");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("c5d6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );

    c.push_back("setboard");
    c.push_back("4k3/8/8/2PpP3/3K4/8/8/8 w - d6 0 2");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("e5d6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_undo_a8b8)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 29: XBoardInterface_undo_a8b8");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result= "r1bqk2r/2pp1ppp/p1n1n3/1p1BP3/8/8/PPP2PPP/RNBQR1K1 b kq - 0 11";
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("setboard");
    c.push_back("r1bqk2r/2pp1ppp/p1n1n3/1p1BP3/8/8/PPP2PPP/RNBQR1K1 b kq - 0 11");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("a8b8");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_do_a8b8)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 30: XBoardInterface_do_a8b8");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result= "1rbqk2r/2pp1ppp/p1n1n3/1p1BP3/8/8/PPP2PPP/RNBQR1K1 w k - 1 12";
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("setboard");
    c.push_back("r1bqk2r/2pp1ppp/p1n1n3/1p1BP3/8/8/PPP2PPP/RNBQR1K1 b kq - 0 11");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("a8b8");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_undo)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 31: XBoardInterface_undo");
    XBoardInterface interpretor;
    vector<string> c;
    int max=68;
    string result="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    string game[]={ "e2e4","e7e5","g1f3","b8c6","f1b5","a7a6","b5a4","b7b5","a4b3","g8f6",
                    "e1g1","f6e4","f1e1","e4c5","b3d5","f8d6","d2d4","c5e6","f3e5","d6e5",
                    "d4e5","a8b8","d5e6","d7e6","d1g4","f7f5","e5f6","d8f6","g4h5","f6g6",
                    "h5g6","h7g6","c1e3","e8g8","b1d2","e6e5","c2c3","c8f5","e3g5","a6a5",
                    "f2f3","f8f7","d2b3","f7d7","b3c5","d7d5","c5a6","b8c8","a1d1","d5d1",
                    "e1d1","f5e6","a2a3","a5a4","d1e1","e6c4","g1f2","c6d8","h2h4","d8f7",
                    "e1d1","c4e6","a6c5","c8e8","d1d3","e6f5","d3d1","f5e6"};

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    for(int i=0;i<max;i++)
    {
        c.push_back(game[i]);
        interpretor.interpret_command(c);
        c.clear();
    }
    for(int i=0;i<max;i++)
    {
        c.push_back("undo");
        interpretor.interpret_command(c);
        c.clear();
    }
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}


BOOST_AUTO_TEST_CASE(XBoardInterface_undo_h7g6)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 32: XBoardInterface_undo_h7g6");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result= "1rb1k2r/2p3pp/p1n1p1Q1/1p6/8/8/PPP2PPP/RNB1R1K1 b k - 0 16";
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("setboard");
    c.push_back("1rb1k2r/2p3pp/p1n1p1Q1/1p6/8/8/PPP2PPP/RNB1R1K1 b k - 0 16");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("h7g6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}

BOOST_AUTO_TEST_CASE(XBoardInterface_do_h7g6)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 33: XBoardInterface_do_h7g6");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result= "1rb1k2r/2p3p1/p1n1p1p1/1p6/8/8/PPP2PPP/RNB1R1K1 w k - 0 17";
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("setboard");
    c.push_back("1rb1k2r/2p3pp/p1n1p1Q1/1p6/8/8/PPP2PPP/RNB1R1K1 b k - 0 16");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("h7g6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}


BOOST_AUTO_TEST_CASE(XBoardInterface_undo2)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 34: XBoardInterface_undo2");
    XBoardInterface interpretor;
    vector<string> c;
    string result="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    string game[]={ "e2e4","e7e5","g1f3","b8c6","f1b5","a7a6","b5a4","b7b5","a4b3","g8f6",
                    "e1g1","f6e4","f1e1","e4c5","b3d5","f8d6","d2d4","c5e6","f3e5","d6e5",
                    "d4e5","a8b8","d5e6","d7e6","d1g4","f7f5","e5f6","d8f6","g4h5","f6g6",
                    "h5g6","h7g6","c1e3","e8g8","b1d2","e6e5","c2c3","c8f5","e3g5","a6a5"};

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("force");
    interpretor.interpret_command(c);
    c.clear();
    for(int i=0;i<22;i++)
    {
        c.push_back(game[i]);
        interpretor.interpret_command(c);
        c.clear();
    }
    for(int i=0;i<22;i++)
    {
        c.push_back("undo");
        interpretor.interpret_command(c);
        c.clear();
    }
    c.push_back("fen");
    interpretor.interpret_command(c);
    BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
}


BOOST_AUTO_TEST_CASE(XBoardInterface_generate_black_king_defense_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 35: XBoardInterface_black_king_defense_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {"c8e6","e8e6","f8e6","g8h8"};
    int sz=sizeof(result)/sizeof((string) "1234");
    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("setboard");
    c.push_back("2b1rnk1/6pp/8/8/2B5/8/8/K7 b - - 0 1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("allmoves");
    interpretor.interpret_command(c);
    split_regex(moves, interpretor.get_last_result(), regex(",") );
    BOOST_CHECK_EQUAL( sz, moves.size() );
    for(int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];
        for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
            if(mv==*it) res=true;
        if(res==false) BOOST_CHECK_EQUAL( mv, "Move not calculated" );
    }
    for (std::vector<string>::iterator it = moves.begin() ; it != moves.end(); ++it)
    {
        bool res=false;
        for(int i=0; i<sz; i++)
            if(*it==result[i]) res=true;
        if(res==false) BOOST_CHECK_EQUAL( "Wrong calculated move", *it );
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_compute_first_black_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 36: XBoardInterface_compute_first_black_moves");
    XBoardInterface interpretor;
    vector<string> c;
    string computed_move;
    string result[]= {"b8a6","b8c6","g8f6","g8h6","a7a6","a7a5","b7b6","b7b5","c7c6","c7c5","d7d6","d7d5","e7e6","e7e5","f7f6","f7f5","g7g6","g7g5","h7h6","h7h5"};
    int sz=sizeof(result)/sizeof((string) "1234");
    bool res=false;

    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.push_back("sd");
    c.push_back("1");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d2d4");
    interpretor.interpret_command(c);
    c.clear();
    computed_move = interpretor.get_last_result();
    for (int i=0;i<sz;i++)
        if( computed_move  == ("move " + result[i]) ) res=true;
    if( !res )
    {
        BOOST_CHECK_EQUAL( "Wrong calculated move", computed_move );
        interpretor.set_output_stream(cout);
        interpretor.display_board();
    }
}

BOOST_AUTO_TEST_CASE(XBoardInterface_compute_undo_first_black_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 37: XBoardInterface_undo_compute_first_black_moves");
    XBoardInterface interpretor;
    vector<string> c;
    string tmove;
    string result="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

    interpretor.set_output_stream(out);
    interpretor.new_game();

    c.push_back("sd");
    c.push_back("6");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("d2d3");
    interpretor.interpret_command(c);
    c.clear();
    tmove=interpretor.get_last_result();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("undo");
    interpretor.interpret_command(c);
    c.clear();
    c.push_back("fen");
    interpretor.interpret_command(c);

    if ( interpretor.get_last_result()!=result )
    {
        BOOST_CHECK_EQUAL( interpretor.get_last_result(), result );
        cout << "Black moved: " << tmove << endl;
        interpretor.set_output_stream(cout);
        interpretor.display_board();
    }
}

BOOST_AUTO_TEST_CASE(TEST_CASE38_XBoardInterface_generate_first_black_moves)
{
    LOG4CPLUS_INFO( logger,"TEST CASE 38: XBoardInterface_generate_first_black_moves");
    XBoardInterface interpretor;
    vector<string> c;
    vector<string> moves;
    string result[]= {  "move b8a6","move b8c6","move g8f6","move g8h6",
                        "move a7a6","move a7a5","move b7b6","move b7b5",
                        "move c7c6","move c7c5","move d7d6","move d7d5",
                        "move e7e6","move e7e5","move f7f6","move f7f5",
                        "move g7g6","move g7g5","move h7h6","move h7h5" };
    int sz=20;
    string tmove;

    interpretor.set_output_stream(out);
    interpretor.new_game();
    c.clear();
    c.push_back("d2d4");
    interpretor.interpret_command(c);
    c.clear();
    tmove=interpretor.get_last_result();
    for (int i=0; i<sz; i++)
    {
        bool res=false;
        string mv=result[i];

        if (mv == tmove)
        {
            BOOST_CHECK_EQUAL( tmove, mv );
            return;
        }
    }
    BOOST_CHECK_EQUAL( tmove, "" );
    cout << "Wrong calculated move: " << tmove;
    interpretor.set_output_stream(cout);
    interpretor.display_board();
}


BOOST_AUTO_TEST_SUITE_END()
