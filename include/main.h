// *** ADDED BY HEADER FIXUP ***
#include <istream>
#include <string>
// *** END ***
#include <iostream>
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/loggingmacros.h>

#include <boost/type_traits.hpp>
#include <boost/token_functions.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>

#define LICENSE \
   "\n	*************************************************************************** \n \
	*   Copyright(C) 2016 by Luc Porchon                                      * \n \
	*   luc.porchon@yahoo.fr                                                  * \n \
	*                                                                         * \n \
	*   This program is free software; you can redistribute it and/or modify  * \n \
	*   it under the terms of the GNU General Public License as published by  * \n \
	*   the Free Software Foundation; either version 2 of the License, or     * \n \
	*  (at your option) any later version.                                    * \n \
	*                                                                         * \n \
	*   This program is distributed in the hope that it will be useful,       * \n \
	*   but WITHOUT ANY WARRANTY; without even the implied warranty of        * \n \
	*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         * \n \
	*   GNU General Public License for more details.                          * \n \
	*                                                                         * \n \
	*   You should have received a copy of the GNU General Public License     * \n \
	*   adouble with this program; if not, write to the                       * \n \
	*   Free Software Foundation, Inc.,                                       * \n \
	*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             * \n \
	***************************************************************************"
