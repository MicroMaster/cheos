/*
 * XBoardInterface.h
 *
 *  Created on: 23 janv. 2012
 *      Author: perso
 */

#ifndef XBOARDINTERFACE_H_
#define XBOARDINTERFACE_H_

#include "chessprotocol.h"

class XBoardInterface: public ChessProtocol
{
public:
    //XBoardInterface();
    //virtual ~XBoardInterface();
    string interpret_command(vector<string> input);
    void new_game();

private:
    void xboard();
    void remove();
    void undo();
    void force();
    string go();
    void level(int mps, int base, int inc);
    string move(const string &mvStr);
    void ping(const string &n);
    void sd(int depth);
    void st(int time);
    void setboard(const string &fen);
    void quit();
    void protover(int);
    bool is_move(string str);
    int parameter_to_int(const string &param);
    static string keyword[];
    //void sigterm();
    //void sigint();
    //void analyze();
    //void computer();
    //void easy();
    //void hard();
    //void ics(string hostname);
    //void name(string x);
    //void nopost();
    //void pause();
    //void post();
    //void rating(int computerRating, int opponentRating);
    //void resume();
    //string bk();
    //string hint();
    //void black();
    //void draw();
    //void move_now();
    //void otim(int n);
    //void playother();
    //void result(string res, string comment);
    //void time(int n);
    //void usermove(string move);
    //void white();
    //void rejected();
    //void variant(string varname);
    //void random();
    //void accepted();

protected:
    bool is_force;
    bool is_xboard=false;
};

#endif /* XBOARDINTERFACE_H_ */
