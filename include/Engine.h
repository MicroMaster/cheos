/*
 * Engine.h
 *
 *  Created on: 24 janv. 2012
 *      Author: perso
 */

#ifndef ENGINE_H_
#define ENGINE_H_

#include <string>
#include <ggtl/core.h>

#include "Board.h"

class Engine
{
public:
    Engine &operator=(const Engine &e);
    Engine();
    Engine(const Engine &e);
    ~Engine();
    void new_game();
    void initialize();
    void free();
    Board &set_move(const std::string &newMove);
    Board &make_move();
    Board &undo_move();
    void set_mode(int key=0, int value=0);
    int get_mode(int key=0); // returns the value
    Board *get_board();
    std::string get_last_move();
    bool is_game_over();
    int get_evaluation();
    void set_time(float t);
    void set_depth(int depth);
    bool is_valid_move(std::string &mv);
    void get_possible_moves(std::list<std::string> &moves);

private:
    Board *my_board=NULL;
    GGTL *my_game=NULL;

    /**
     * my_pointer_to_board is a pointer on Board object to map the Wrapper methods
     * to required GGTL functions.
     */
    void *my_pointer_to_board;
    GGTL_MOVE *move;
    GGTL_VTAB *get_vector_table();

    Move *allocate_move();
    Move *string_to_move(const std::string &move_string);
    //std::string move_to_string(const Move mv);
    std::list<Move> get_possible_moves();

    EngineException manage_error(const std::string &methodName="unknown method", const std::string &message="unknown error",int errorCode=0, log4cplus::LogLevel level=log4cplus::ERROR_LOG_LEVEL);

    static void * wrapper_to_call_make_move(void *pointerToBoard, void *move, GGTL *game);
    static void * wrapper_to_call_undo_move(void *pointerToBoard, void *move, GGTL *game);
    static GGTL_MOVE * wrapper_to_call_generate_moves(void *pointerToBoard, GGTL *game);
    static int wrapper_to_call_evaluate(void *pointerToBoard, GGTL *game);
    static int wrapper_to_call_game_over(void *my_pointer_to_board, GGTL *game);
};
#endif /* ENGINE_H_ */
