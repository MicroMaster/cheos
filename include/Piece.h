/*
 * piece.h
 *
 *  Created on: 27 janv. 2012
 *      Author: perso
 */

#ifndef PIECE_H_
#define PIECE_H_


enum piece
{
    PAWN      = 100,
    KNIGHT    = 300,
    BISHOP    = 320,
    ROOK      = 500,
    QUEEN     = 900,
    KING      = 32000
};

enum white_piece
{
    W_PAWN      = 100,
    W_KNIGHT    = 300,
    W_BISHOP    = 320,
    W_ROOK      = 500,
    W_QUEEN     = 900,
    W_KING      = 32000
};

enum black_piece
{
    B_KING      = -32000,
    B_QUEEN     = -900,
    B_ROOK      = -500,
    B_BISHOP    = -320,
    B_KNIGHT    = -300,
    B_PAWN      = -100
};

#endif /* PIECE_H_ */
