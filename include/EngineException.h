/*
 * EngineException.h
 *
 *  Created on: 23 janv. 2012
 *      Author: perso
 */

#ifndef ENGINEEXCEPTION_H_
#define ENGINEEXCEPTION_H_

#include <exception>
#include <string>
#include <log4cplus/logger.h>

class EngineException: public std::exception {
public:
	EngineException(int number,const char* phrase, log4cplus::LogLevel level) throw();
	virtual const char *what() const throw() {
	        return m_phrase;
	}
	int getLevel() const throw();
	virtual ~EngineException() throw() {}

private:
    int m_number;               	//Exception number
    const char* m_phrase;       	//Exception Description
    log4cplus::LogLevel m_level;    //Exception level

};

#endif /* ENGINEEXCEPTION_H_ */
