/*
 * chessprotocol.h
 *
 *  Created on: 22 janv. 2012
 *      Author: perso
 */

#ifndef CHESSPROTOCOL_H_
#define CHESSPROTOCOL_H_

#include "Engine.h"


/**
Most computer chess programs are divided into an engine (which computes the best move given a current position) and a user interface. Most engines are separate programs from the user interface, and the two parts communicate to each other using a public communication protocol. The most popular protocol is the Xboard/Winboard Communication protocol. Another open alternate chess communication protocol is the Universal Chess Interface. By dividing chess programs into these two pieces, developers can write only the user interface, or only the engine, without needing to write both parts of the program. (See also List of chess engines.)

	@author Luc Porchon <luc.porchon@wizeo.fr>
*/

using namespace std;

class ChessProtocol{
public:
    ChessProtocol();
    ~ChessProtocol();

    vector<string> &get_output_command();
    void set_engine(Engine &e);
    void clear_output_command();
    Engine &get_engine();
    void set_output_stream(ostream &o);
    void display_board();
    void display_fen();
    int evaluate();
    void all_moves();
    void display_prompt();
    void output_protocol(const string &st);
    string get_last_result(void);
    void quit();

protected:
    Engine *my_chess_engine=NULL;
    vector<string> *output_command;
    ostream *my_output_stream;
    string my_result;
};




#endif /* CHESSPROTOCOL_H_ */
