// *** ADDED BY HEADER FIXUP ***
#include <string>
// *** END ***
/*
 * Move.h
 *
 *  Created on: 28 janv. 2012
 *      Author: perso
 */

#include<string>

#ifndef MOVE_H_
#define MOVE_H_

struct Move
{
    int from = 0;
    int to = 0;
    int promotion = 0;	// Promoted piece
    int taken = 0;      // The piece that has been taken. Used to undo the move
    std::string brd="";
};

inline int raw(register int idx)
{
    return (idx - 24) / 12;
}

inline int column(register int idx)
{
    return idx % 12 - 2;
}

// 1<=x<=8 1<=y<=8
inline int idx(register int x, register int y)
{
    return y * 12 + 13 + x;
}

// 0<=x<=7 0<=y<=7
inline int idx7(register int x, register int y)
{
    return y * 12 + 26 + x;
}

inline std::string move_to_string(Move mv)
{
    std::string s_move;
    s_move ='a' + column(mv.from);
    s_move+='1' + raw(mv.from);
    s_move+='a' + column(mv.to);
    s_move+='1' + raw(mv.to);
    if(mv.promotion!=0)
    {
        switch (mv.promotion)
        {
        case W_QUEEN:
        case B_QUEEN:
            {
                s_move+='q';
                break;
            }
        case W_ROOK:
        case B_ROOK:
            {
                s_move+='r';
                break;
            }
        case W_BISHOP:
        case B_BISHOP:
            {
                s_move+='b';
                break;
            }
        case W_KNIGHT:
        case B_KNIGHT:
            {
                s_move+='n';
                break;
            }

        }
    }
    return s_move;
}

#endif /* MOVE_H_ */
