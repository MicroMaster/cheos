/*
 * Board.h
 *
 *  Created on: 27 janv. 2012
 *      Author: perso
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <list>

#include "Piece.h"
#include "Move.h"
#include "EngineException.h"

#include <log4cplus/loggingmacros.h>

const std::string algebric[8][8]=
{
    {"a1","a2","a3","a4","a5","a6","a7","a8"},
    {"b1","b2","b3","b4","b5","b6","b7","b8"},
    {"c1","c2","2c3","c4","c5","c6","c7","c8"},
    {"d1","d2","d3","d4","d5","d6","d7","d8"},
    {"e1","e2","e3","e4","e5","e6","e7","e8"},
    {"f1","f2","f3","f4","f5","f6","f7","f8"},
    {"g1","g2","g3","g4","g5","g6","g7","g8"},
    {"h1","h2","h3","h4","h5","h6","h7","h8"}
};

const int short_mailbox[64] = {110, 111, 112, 113, 114, 115, 116, 117,
                                98,	 99,  100, 101, 102, 103, 104, 105,
                                86,	 87,  88,  89,	90,	 91,  92,  93,
                                74,	 75,  76,  77,  78,  79,  80,  81,
                                62,	 63,  64,  65,	66,	 67,  68,  69,
                                50,	 51,  52,  53,	54,	 55,  56,  57,
                                38,	 39,  40,  41,	42,	 43,  44,  45,
                                26,	 27,  28,  29,	30,	 31,  32,  33 };

const signed int CHANGE_SIDE = -1;

enum check
{
    CHECKMATE = 25500, CHECK = 0
};

enum mailbox_limit
{
    MAX_MAILBOX = 144,  MIN_SQUARE = 26, MAX_SQUARE = 117
};

enum color
{
    WHITE = 1, BLACK = -1
};

enum square_status
{
    EMPTY = 0, BORDER = 255,
    PAWN_CONTROL = 1, BISHOP_CONTROL = 3, KNIGHT_CONTROL = 3, ROOK_CONTROL = 5, QUEEN_CONTROL = 9, CONTROL = 1,
    CENTER_CONTROL = 2
};

enum board_limit
{
    MAX_RAW = 8, MAX_FILE = 8, BOARD_SIZE = 64
};


class Board
{
public:
    log4cplus::Logger logger_;

    /**
     * Active color.
     */
    int player_to_move;

    Board();
    Board(const Board &b);
    ~Board();

    Board &operator=(const Board &b);

    /**
     * Clean up the board
     */
    void clear();

    /**
     * Set the standard initial position
     */
    void initialize();

    /**
     * Make the move mv on the board
     * @param *mv
     */
    void make_move(Move &mv);

    /**
     * Unmake the move mv on the board
     * @param *mv
     */
    void undo_move(const Move &mv);

    /**
     * Evaluate the current position
     */
    int evaluate();

    /*!
     \fn Board::get_all_moves(list<string> moves)
     */
    void get_all_moves(std::list<std::string> &moves);

    std::string to_string();

    void set_fen(const std::string &fen_pos);

    std::string get_fen(void);

    /**
     * This square is it attacked ? true=yes
     * @param position of the square in the mailbox
     */
    bool is_checked(int pos, int from = 0, int to=0);

    /**
     * Is this king attacked ? true=yes
     * @param king's color
     */
    bool is_king_checked(int color);

    /**
     * Is it a checkmate ?
     */
    int is_checkmate(int color);

    /**
     * Moves generator.
     * @param list of possible moves from the actual board position.
     */
    void generate_all_moves(std::list<Move> &moves);

private:
    /**
     * Castling availability.White can castle kingside, White can castle queenside,
     * Black can castle kingside, Black can castle queenside.
     */
    int     white_castle_king_side=0, white_castle_queen_side=0,
            black_castle_king_side=0, black_castle_queen_side=0;

    /**
     * If a pawn has just made a two-square move, this is the position "behind" the pawn.
     * This is recorded regardless of whether there is a pawn in position to make an en passant capture
     */
    int en_passant_square;

    /**
     * Halfmove clock: This is the number of halfmoves since the last pawn advance or capture.
     * This is used to determine if a draw can be claimed under the fifty-move rule.
     */
    int halfmove_clock;

    /**
     * Fullmove number: The number of the full move. It starts at 1, and is incremented
     * after Black's move.
     */
    int fullmove_number;

    /**
     * Kings' position mainly used to know whether there is a check or not
     */
    int white_king_pos;
    int black_king_pos;


    /**
     * The chessboard as a mailbox
     */
    int mailbox[MAX_MAILBOX];

    /**
     * Piece move generators
     */
    inline void pawn_moves      (register int pos, std::list<Move> &moves);
    inline void knight_moves    (register int pos, std::list<Move> &moves);
    inline void bishop_moves    (register int pos, std::list<Move> &moves);
    inline void rook_moves      (register int pos, std::list<Move> &moves);
    inline void king_moves      (register int pos, std::list<Move> &moves);

    /**
     * Get the square list that might be used to protect the king if he is checked
     */
    void get_checked_squares(register int player, std::list<int> &squares);

    int space_value(register int pos = 0);

};

#endif /* BOARD_H_ */
